/**
Strs library, v.1.0.3

Copyright 2019 Third Pin LLC

Permission is hereby granted, free of charge, to any person obtaining 
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
**/

#pragma once

#ifdef STRS_ETL_SUPPORT
	#include <etl/string_view.h>
#elif __cplusplus >= 201703L
	#include <string_view>
	#define STRS_STD_STRING_VIEW_AVAILABLE
#else
	#define STRS_NO_STRING_VIEW_SUPPORT
#endif

namespace strs {

#ifndef STRS_NO_STRING_VIEW_SUPPORT

	#ifdef STRS_ETL_SUPPORT
		using StringView = etl::string_view;
	#elif __cplusplus >= 201703L
		using StringView = std::string_view;
	#else
		#define STRS_NO_STRING_VIEW_SUPPORT
	#endif

#endif
}
