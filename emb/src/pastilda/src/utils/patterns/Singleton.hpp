#pragma once

#include <cstdint>
#include <new>
#include <type_traits>

#include <utils/patterns/NonCopyable.hpp>
#include <utils/patterns/NonMovable.hpp>

namespace utils {

namespace patterns {

/**
 * @brief Inherit this class to make a child class a singleton
 *
 * If you make a private child constructor you should make this class a friend
 * of yours.
 *
 * For example:
 *
 * class A : public Singleton<A> {
 *     friend class Singleton<A>; // we need it to call A constructor
 *     							  // from Singleton<A> constructor.
 *
 *     private:
 *        A() {} // nobody should allowed to use private A constructor
 *        		 // but Singleton<A> because it is a friend of A
 *        		 // For over guys from outside Singleton<A> provides a
 *        		 // public static function instance().
 * }
 *
 */
template<typename T>
struct Singleton
  : NonMovable<Singleton<T>>
  , NonCopyable<Singleton<T>>
{
    static T& Instance()
    {
        static std::uint8_t buffer[sizeof(T)];
        // Use placement new to prevent destructor problems
        static T* object = new (&buffer) T;
        return *object;
    }

    Singleton() = default;
    Singleton(const Singleton&) = delete;
    Singleton(Singleton&&) = delete;
    Singleton& operator=(const Singleton&) = delete;
    Singleton& operator=(Singleton&&) = delete;
};

}  // namespace patterns

}  // namespace utils
