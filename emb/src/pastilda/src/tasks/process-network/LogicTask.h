/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKS_LOGICTASK_H_
#define TASKS_LOGICTASK_H_

#include <config/TasksConfig.h>
#include <etl/cstring.h>
#include <etl/vector.h>
#include <lang/Ascii.h>
#include <tree/TreeImpl.hpp>
#include <tree/TreeNavigator.hpp>

#include "DatabaseTask.h"
#include "OutputManagerTask.h"
#include "SwitcherTask.h"
#include "common/IKahnProcessTask.hpp"
#include "common/Message.hpp"

namespace tasks {

namespace usbkbd = hw::kbd;

namespace msg {

enum class LogicEvent : uint8_t
{
    PASTILDA_MODE,
    ASCII_KEY,
    DB_READY,
    DB_ERROR,
    DISP_ENDED,
    LOGIN_ENDED,
};

using LogicData = char;
using LogicMessage = common::Message<LogicEvent, LogicData>;

} /* namespace msg */

class LogicTask final
  : public common::IKahnProcessTask<config::LogicTask::NAME,
                                    msg::LogicMessage,
                                    config::LogicTask::FIFO_SIZE>
{
 public:
    using Password = strs::StringFixed<config::LogicTask::MAX_PASSWORD_SIZE>;
    using Key = utils::kbd::KeyObj;

    LogicTask() :
      IKahnProcessTask(config::LogicTask::STACK_SIZE,
                       config::LogicTask::PRIORITY),
      _tree(&Tree::Instance()),
      _navigator(_tree)
    {

        this->Start();
    };
    virtual ~LogicTask() = default;

    void Run();

 private:
    using Tree = tree::TreeImpl;
    using Navigator = tree::TreeNavigator<Tree>;

    enum State
    {
        IDLE,
        INVITATION,
        AUTH,
        DECRYPT,
        DB_WAIT,
        DB_ERROR,
        MENU_START,
        NAVIGATION,
        LOGIN,
        LOGIN_WAIT,
        ENDING,
        END
    };

    enum StateAction
    {
        STAY,
        NEXT,
        FAIL,
        QUIT,
    };

    State _state = State::IDLE;
    Password _password;
    Key _key;
    Tree* _tree;
    Navigator _navigator;

    void _state_machine_proc();

    StateAction _idle_proc();
    StateAction _invitation_proc();
    StateAction _auth_proc();
    StateAction _decrypt_proc();
    StateAction _db_wait_proc();
    StateAction _db_error_proc();
    StateAction _menu_start_proc();
    StateAction _navigation_proc();
    StateAction _login_proc();
    StateAction _login_wait_proc();
    StateAction _ending_proc();
    StateAction _end_proc();

    void _display_node_name();
};

} /* namespace tasks */

#endif /* TASKS_LOGICTASK_H_ */
