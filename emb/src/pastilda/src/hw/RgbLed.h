/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HW_RGBLED_H_
#define HW_RGBLED_H_

#include <config/HwConfig.h>

#include "Led.h"

namespace hw {

class RgbLed final
{
 public:
    RgbLed();
    ~RgbLed() = default;

    enum class Color : uint8_t
    {
        RED = 0b001,
        GREEN = 0b010,
        BLUE = 0b100,
        MAGENTA = RED | BLUE,
        CYAN = BLUE | GREEN,
        YELLOW = RED | GREEN,
        WHITE = RED | GREEN | BLUE
    };

    void on(Color color);
    void off();

    void circle();
    void set_start_color(Color color);

 private:
    constexpr static uint8_t LEDS_COUNT = 3;
    constexpr static uint8_t COLORS_COUNT = 7;

    Led _leds[LEDS_COUNT];
    Color _current_color = Color::RED;
};

} /* namespace hw */

#endif /* HW_RGBLED_H_ */
