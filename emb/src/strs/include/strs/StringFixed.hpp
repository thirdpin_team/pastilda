/**
Strs library, v.1.0.3

Copyright 2019 Third Pin LLC

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
**/

#pragma once

#include <cstddef>
#include <cstdint>
#include <cstring>
#include <string.h>

#include <algorithm>
#include <iterator>
#include <string>
#include <type_traits>

#include "StringConst.hpp"
#include "StringView.hpp"

namespace strs {

namespace detail {

template<typename T>
struct no_specifier_for_this_type;

// Default value is not valid (not compile for not specified type)
// Specifiers for singed char type not available in order to avoiding ambiguity!
// Use StringFixed::add(char c) or StringFixed::format(...).
template<typename T>
inline constexpr strs::StringConst specifier = no_specifier_for_this_type<T>::value;

#ifdef STRS_PRINTF_UNSIGNED_CHAR_SUPPORT
template<>
inline constexpr strs::StringConst specifier<unsigned char> = "%hhu";
#endif

template<>
inline constexpr strs::StringConst specifier<int> = "%d";

template<>
inline constexpr strs::StringConst specifier<unsigned int> = "%u";

template<>
inline constexpr strs::StringConst specifier<short int> = "%d";

template<>
inline constexpr strs::StringConst specifier<unsigned short int> = "%u";

template<>
inline constexpr strs::StringConst specifier<long int> = "%l";

template<>
inline constexpr strs::StringConst specifier<unsigned long int> = "%lu";

#ifdef STRS_PRINTF_LONG_LONG_SUPPORT
template<>
inline constexpr strs::StringConst specifier<long long int> = "%lld";

template<>
inline constexpr strs::StringConst specifier<unsigned long long int> = "%llu";
#endif

#ifdef STRS_PRINTF_FLOAT_SUPPORT
template<>
inline constexpr strs::StringConst specifier<float> = "%g";

template<>
inline constexpr strs::StringConst specifier<double> = "%g";
#endif
}  // namespace detail

template<std::size_t N>
class StringFixed
{
 public:
    constexpr static std::size_t CAPACITY = N + 1;
    constexpr static std::size_t USEFUL_CAPACITY = N;

    using CharType = char;

    typedef CharType value_type;
    typedef CharType& reference;
    typedef const CharType& const_reference;
    typedef CharType* pointer;
    typedef const CharType* const_pointer;
    typedef CharType* iterator;
    typedef const CharType* const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
    typedef size_t size_type;

    enum Status
    {
        OK,
        BAD_ALLOC,
        EMPTY,
        BAD_FORMAT
    };

    /******************************************
     * Constructors ***************************
     ******************************************/

    // With test
    StringFixed() : _length(1) { _buffer[0] = '\0'; }

    // With test
    StringFixed(char c) : _length(2)
    {
        _buffer[0] = c;
        _buffer[1] = '\0';
    }

    // With test
    StringFixed(const char* s, std::size_t length) { set(s, length); }

    // With test
    StringFixed(const StringConst& s) { set(s.data(), s.length()); }

    // With test
    template<std::size_t str_length>
    StringFixed(const char (&s)[str_length]) : _length(str_length)
    {
        set(s);
    }

    // With test
    template<std::size_t M>
    StringFixed(StringFixed<M> const& src) : _length(0)
    {
        set(src);
    }

#ifndef STRS_NO_STRING_VIEW_SUPPORT
    // With test
    StringFixed(StringView const& src) { set(src.data(), src.length()); }
#endif

    // With test
    StringFixed(StringFixed const& src) : _length(0) { set(src.data(), src.length()); }

    StringFixed(StringFixed&&) = default;

    /******************************************
     * Destructor *****************************
     ******************************************/

    ~StringFixed() = default;

    /******************************************
     * Static method-constructors *************
     ******************************************/

    static StringFixed<N> from_cstring(const char* cstr)
    {
        return StringFixed<N>(cstr, _strlen_save(cstr, USEFUL_CAPACITY));
    }

    /******************************************
     * Operator= ******************************
     ******************************************/

    StringFixed& operator=(StringFixed&&) = default;

    // With test
    template<std::size_t str_length>
    StringFixed& operator=(const char (&s)[str_length])
    {
        set(s);
        return *this;
    }

    // With test
    template<std::size_t M>
    StringFixed& operator=(StringFixed<M> const& src)
    {
        set(src.data(), src.length());
        return *this;
    }

#ifndef STRS_NO_STRING_VIEW_SUPPORT
    // With test
    StringFixed& operator=(const StringView& src)  // ref is need to prevent
    {                                              // using a c-string init
        set(src.data(), src.length());
        return *this;
    }

    // With test
    operator StringView() const { return StringView(data(), length()); }
#endif

    /******************************************
     * set ************************************
     ******************************************/

    // With test
    StringFixed& operator=(StringFixed const& src)
    {
        set(src.data(), src.length());
        return *this;
    }

    // With test
    Status set(char c) { return set(_to_pointer(&c), 1); }

    // With test
    Status set_cstring(const char* s) { return set(s, _strlen_save(s, USEFUL_CAPACITY)); }

    // With test
    template<std::size_t M>
    Status set(StringFixed<M> const& src)
    {
        return set(src.data(), src.length());
    }

#ifndef STRS_NO_STRING_VIEW_SUPPORT
    // With test
    Status set(StringView const& src) { return set(src.data(), src.length()); }
#endif

    // With test
    Status set(const char* s, std::size_t length)
    {
        auto status = OK;
        if (length > USEFUL_CAPACITY) {
            status = BAD_ALLOC;
            length = USEFUL_CAPACITY;
        }

        _length = length + 1;
        std::copy(s, s + length, begin());
        _buffer[_useful_length()] = '\0';

        return status;
    }

    // With test
    template<std::size_t str_length>
    Status set(const char (&s)[str_length])
    {
        _length = str_length;

        auto status = OK;
        if (_length - 1 > USEFUL_CAPACITY) {
            status = BAD_ALLOC;
            _length = USEFUL_CAPACITY;
        }

        std::copy(s, s + _length, begin());
        _buffer[_useful_length()] = '\0';

        return status;
    }

    /******************************************
     * Adding stuff ***************************
     ******************************************/

    // With test
    template<std::size_t str_length>
    StringFixed& operator+=(const char (&s)[str_length])
    {
        add(s);
        return *this;
    }

    // With test
    template<std::size_t M>
    StringFixed& operator+=(StringFixed<M> const& src)
    {
        add(src.data(), src.length());
        return *this;
    }

    // With test
    StringFixed& operator+=(StringConst const& src)
    {
        add(src.data(), src.length());
        return *this;
    }

#ifndef STRS_NO_STRING_VIEW_SUPPORT
    // With test
    StringFixed& operator+=(StringView const& src)
    {
        add(src.data(), src.length());
        return *this;
    }
#endif

    // With test
    Status add(char c) { return add(&c, 1); }

#ifdef STRS_STREAM_CHAR_AS_SYMBOL
    // With test
    StringFixed<N>& operator<<(char c)
    {
        this->add(c);
        return *this;
    }
#endif

    // With test
    template<std::size_t str_length>
    Status add(const char (&s)[str_length])
    {
        return add(_to_cpointer(s), str_length - 1);
    }

    // With test
    template<std::size_t str_length>
    StringFixed<N>& operator<<(const char (&s)[str_length])
    {
        this->add(_to_cpointer(s), str_length - 1);
        return *this;
    }

    // With test
    Status add_cstring(const char* s) { return add(s, _strlen_save(s, USEFUL_CAPACITY)); }

    // With test
    template<std::size_t M>
    Status add(StringFixed<M> const& src)
    {
        return add(src.begin(), src.length());
    }

    // With test
    template<std::size_t M>
    StringFixed<N>& operator<<(StringFixed<M> const& src)
    {
        this->add(src.begin(), src.length());
        return *this;
    }

    // With test
    Status add(StringConst src) { return add(src.data(), src.length()); }

    // With test
    StringFixed<N>& operator<<(StringConst src)
    {
        this->add(src.data(), src.length());
        return *this;
    }

#ifndef STRS_NO_STRING_VIEW_SUPPORT
    // With test
    Status add(StringView const& src) { return add(src.data(), src.length()); }

    // With test
    StringFixed<N>& operator<<(StringView const& src)
    {
        this->add(src.data(), src.length());
        return *this;
    }
#endif

#ifndef STRS_NO_STRING_VIEW_SUPPORT
    // With test
    template<class... Args>
    Status format(StringView const& fmt, Args... args)
    {
        uint32_t remain_length = CAPACITY - _length;  // without null term

        auto start_pos = end();
        auto written = snprintf(start_pos, remain_length + 1, fmt.cbegin(), args...);
        if (written < 0) {
            return Status::BAD_FORMAT;
        }

        _length += (written > (int)remain_length ? (int)remain_length : written);

        return Status::OK;
    }

    // With test
    template<class... Args>
    Status formatn(StringView const& fmt, std::size_t max_length, Args... args)
    {
        uint32_t remain_length = CAPACITY - _length;  // without null term
        max_length = max_length > remain_length ? remain_length : max_length;

        auto start_pos = end();
        auto written = snprintf(start_pos, max_length + 1, fmt.cbegin(), args...);
        if (written < 0) {
            return Status::BAD_FORMAT;
        }

        _length += (written > (int)max_length ? (int)max_length : written);
        return Status::OK;
    }
#else
    // With test
    template<class... Args>
    Status format(const char* fmt, Args... args)
    {
        uint32_t remain_length = CAPACITY - _length;  // without null term

        auto written = snprintf(end(), remain_length + 1, fmt, args...);
        if (written < 0) {
            return Status::BAD_FORMAT;
        }

        _length += (written > (int)remain_length ? (int)remain_length : written);

        return Status::OK;
    }

    // With test
    template<class... Args>
    Status formatn(const char* fmt, std::size_t max_length, Args... args)
    {
        uint32_t remain_length = CAPACITY - _length;  // without null term
        max_length = max_length > remain_length ? remain_length : max_length;

        auto written = snprintf(end(), max_length + 1, fmt, args...);
        if (written < 0) {
            return Status::BAD_FORMAT;
        }

        _length += (written > (int)max_length ? (int)max_length : written);
        return Status::OK;
    }
#endif

    // With test
    template<typename T>
    StringFixed<N>& operator<<(T val)
    {
        static_assert(std::is_fundamental<T>::value, "Primitive type required");

        uint32_t remain_length = CAPACITY - _length;  // without null term
        auto written = snprintf(end(), remain_length + 1, detail::specifier<T>, val);
        if (written > 0)
            _length += (written > (int)remain_length ? (int)remain_length : written);

        return *this;
    }

    // With test
    Status add(const char* s, std::size_t length)
    {
        if (_length + length - 1 > USEFUL_CAPACITY) {
            return BAD_ALLOC;
        }

        std::copy(s, s + length, end());
        _length += length;
        _buffer[_useful_length()] = '\0';

        return OK;
    }

    // With test
    Status pop_back()
    {
        if (_useful_length() > 0) {
            _length--;
            _buffer[_useful_length()] = '\0';

            return OK;
        }
        else {
            return EMPTY;
        }
    }

    /******************************************
     * Access stuff ***************************
     ******************************************/

    // With test
    const_reference back() { return *rbegin(); }

    const_reference operator[](std::size_t i) { return _buffer[i]; }

    const_pointer data() const { return _to_pointer(_buffer); }

    operator char*() { return _to_pointer(_buffer); }

    operator const char*() const { return _to_pointer(_buffer); }

    pointer data() { return _to_pointer(_buffer); }

    std::size_t length() const { return _length - 1; }

    iterator begin() { return _to_pointer(_buffer); }

    const_iterator begin() const { return _to_pointer(_buffer); }

    const_iterator cbegin() const { return _to_pointer(_buffer); }

    iterator end() { return &_buffer[_useful_length()]; }

    const_iterator end() const { return &_buffer[_useful_length()]; }

    const_iterator cend() const { return &_buffer[_useful_length()]; }

    reverse_iterator rbegin() { return reverse_iterator(end()); }

    const_reverse_iterator rbegin() const { return const_reverse_iterator(end()); }

    const_reverse_iterator crbegin() const { return const_reverse_iterator(end()); }

    reverse_iterator rend() { return reverse_iterator(begin()); }

    const_reverse_iterator rend() const { return const_reverse_iterator(begin()); }

    const_reverse_iterator crend() const { return const_reverse_iterator(begin()); }

    /******************************************
     * Status stuff ***************************
     ******************************************/

    bool full() const { return _useful_length() == USEFUL_CAPACITY; }

    // With test
    bool empty() const { return _useful_length() == 0; }

    /******************************************
     * Compare stuff **************************
     ******************************************/

    template<std::size_t M>
    int compare(const StringFixed<M>& other) const
    {
        return _compare(this->begin(), this->end(), other.begin(), other.end());
    }

    int compare_cstring(const char* other) const
    {
        return _compare(this->begin(), this->end(), other,
                        other + _strlen_save(other, USEFUL_CAPACITY));
    }

    template<std::size_t str_length>
    int compare(const char (&other)[str_length]) const
    {
        return _compare(this->begin(), this->end(), other, other + str_length - 1);
    }

    int compare(const char* other, std::size_t length) const
    {
        return _compare(this->begin(), this->end(), other, other + length);
    }

    /******************************************
     * Other stuff ****************************
     ******************************************/

    void update_length()
    {
        _length = _strlen_save(_buffer, USEFUL_CAPACITY);
        if (_length != USEFUL_CAPACITY)
            _length += 1;  // for '\0'
    }

    void resize(std::size_t size)
    {
        if (size <= _useful_length()) {
            _length = size + 1;
        }
        else {
            _length = CAPACITY;
        }
        _buffer[_useful_length()] = '\0';
    }

    void clear()
    {
        _length = 1;
        _buffer[0] = '\0';
    }

 private:
    CharType _buffer[CAPACITY];
    std::size_t _length;

    inline std::size_t _useful_length() const { return _length - 1; }

    template<typename T>
    inline pointer _to_pointer(T ptr)
    {
        return reinterpret_cast<pointer>(ptr);
    }

    template<typename T>
    inline const_pointer _to_pointer(T ptr) const
    {
        return reinterpret_cast<const_pointer>(ptr);
    }

    template<typename T>
    inline const_pointer _to_cpointer(T ptr) const
    {
        return reinterpret_cast<const_pointer>(ptr);
    }

    int _compare(const_pointer begin_iter1,
                 const_pointer end_inter1,
                 const_pointer begin_iter2,
                 const_pointer end_inter2) const
    {
        auto it1 = begin_iter1;
        auto it2 = begin_iter2;

        while ((it1 != end_inter1) && (it2 != end_inter2)) {
            ++it1;
            ++it2;
        }

        if ((it1 == end_inter1) && (it2 == end_inter2)) {
            return 0;
        }
        else if (it2 == end_inter2) {
            return -1;
        }
        else {
            return 1;
        }
    }

    static std::size_t _strlen_save(const char* s, size_t maxlen)
    {
        const char* e = s;
        std::size_t n = 0;
        for (e = s, n = 0; *e && n < maxlen; e++, n++) {}
        return n;
    }
};

// With test
template<std::size_t N>
bool operator==(const StringFixed<N>& lhs, const StringFixed<N>& rhs)
{
    return (lhs.length() == rhs.length()) &&
           std::equal(lhs.begin(), lhs.end(), rhs.begin());
}

// With test
template<std::size_t N, std::size_t M>
bool operator==(const StringFixed<N>& lhs, const StringFixed<M>& rhs)
{
    return (lhs.length() == rhs.length()) &&
           std::equal(lhs.begin(), lhs.end(), rhs.begin());
}

// With test
template<std::size_t N, std::size_t M>
bool operator==(const StringFixed<N>& lhs, const char (&rhs)[M])
{
    return (lhs.length() == std::strlen(rhs)) && std::equal(lhs.begin(), lhs.end(), rhs);
}

// With test
template<std::size_t N, std::size_t M>
bool operator==(const char (&lhs)[M], const StringFixed<N>& rhs)
{
    return (rhs.length() == std::strlen(lhs)) && std::equal(rhs.begin(), rhs.end(), lhs);
}

// With test
template<std::size_t N>
bool operator!=(const StringFixed<N>& lhs, const StringFixed<N>& rhs)
{
    return !(lhs == rhs);
}

// With test
template<std::size_t N, std::size_t M>
bool operator!=(const StringFixed<N>& lhs, const StringFixed<M>& rhs)
{
    return !(lhs == rhs);
}

// With test
template<std::size_t N, std::size_t M>
bool operator!=(const StringFixed<N>& lhs, const char (&rhs)[M])
{
    return !(lhs == rhs);
}

// With test
template<std::size_t N, std::size_t M>
bool operator!=(const char (&lhs)[M], const StringFixed<N>& rhs)
{
    return !(lhs == rhs);
}

template<std::size_t N>
bool operator>(const StringFixed<N>& lhs, const StringFixed<N>& rhs)
{
    return (strncmp(lhs.data(), rhs.data(), N) > 0);
}

template<std::size_t N>
bool operator<(const StringFixed<N>& lhs, const StringFixed<N>& rhs)
{
    return (rhs > lhs);
}

#ifdef STRS_STD_STRING_VIEW_AVAILABLE
// With test
template<std::size_t N>
bool operator==(const StringFixed<N>& lhs, const std::string_view& rhs)
{
    return (lhs.length() == rhs.length()) &&
           std::equal(lhs.begin(), lhs.end(), rhs.begin());
}

// With test
template<std::size_t N>
bool operator==(const std::string_view& lhs, const StringFixed<N>& rhs)
{
    return (lhs.length() == rhs.length()) &&
           std::equal(lhs.begin(), lhs.end(), rhs.begin());
}

template<std::size_t N>
bool operator!=(const StringFixed<N>& lhs, const std::string_view& rhs)
{
    return !(lhs == rhs);
}

template<std::size_t N>
bool operator!=(const std::string_view& lhs, const StringFixed<N>& rhs)
{
    return !(lhs == rhs);
}
#endif

#ifdef STRS_ETL_SUPPORT
template<std::size_t N>
bool operator==(const StringFixed<N>& lhs, const etl::string_view& rhs)
{
    return (lhs.length() == rhs.length()) &&
           std::equal(lhs.begin(), lhs.end(), rhs.begin());
}

template<std::size_t N>
bool operator==(const etl::string_view& lhs, const StringFixed<N>& rhs)
{
    return (lhs.length() == rhs.length()) &&
           std::equal(lhs.begin(), lhs.end(), rhs.begin());
}

template<std::size_t N>
bool operator!=(const StringFixed<N>& lhs, const etl::string_view& rhs)
{
    return !(lhs == rhs);
}

template<std::size_t N>
bool operator!=(const etl::string_view& lhs, const StringFixed<N>& rhs)
{
    return !(lhs == rhs);
}
#endif

}  // namespace strs
