#ifndef SRC_PASTILDA_SRC_APP_APP_HPP_
#define SRC_PASTILDA_SRC_APP_APP_HPP_

#include <queue.hpp>
#include <thread.hpp>
#include <ticks.hpp>

#include <tasks/LedBlinkingTask.h>
#include <tasks/UsbDeviceTask.h>
#include <tasks/UsbHostTask.h>
#include <tasks/WatchdogTask.h>
#include <tasks/process-network/LogicTask.h>
#include <tasks/process-network/SwitcherTask.h>
#include <utils/patterns/NonCopyable.hpp>
#include <utils/patterns/NonMovable.hpp>

namespace app {

/**
 * Class which embeds all tasks and starts the scheduler
 */
class App final
  : public utils::patterns::NonCopyable<App>
  , public utils::patterns::NonMovable<App>
{
 public:
    /**
     * A constructor
     *
     * Create all tasks
     */
    explicit App();

    /**
     * A destructor
     */
    ~App() = default;

    /**
     * Start the RTOS scheduler and stuck in an IDLE infinitely
     *
     * @return standard success return code (0) - never be returned
     */
    int exec() const;

 private:
    const tasks::LedBlinkingTask led_blinking_task;
    const tasks::UsbDeviceTask& usb_device_task;
    const tasks::UsbHostTask& usb_host_task;
    const tasks::SwitcherTask switcher_task;
    const tasks::LogicTask logic_task;
    const tasks::OutputManagerTask output_task;
    const tasks::DatabaseTask db_task;

    // const tasks::WatchdogTask watchdog_task;
};

}  // namespace app

#endif /* SRC_PASTILDA_SRC_APP_APP_HPP_ */
