/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_MEMORY_DICT_HPP_
#define UTILS_MEMORY_DICT_HPP_

#include <etl/flat_map.h>
#include <etl/unordered_map.h>

namespace utils {

namespace memory {

namespace detail {

template<class TMap>
class DictBase
{
 public:
    using DictType = TMap;
    using Key = typename DictType::key_type;
    using Value = typename DictType::mapped_type;
    static constexpr std::size_t SIZE = DictType::MAX_SIZE;

    using Pair = std::pair<Key, Value>;

    using iterator = typename DictType::iterator;
    using const_iterator = typename DictType::const_iterator;

    DictBase() = default;
    ~DictBase() = default;

    const_iterator get(const Key& key) const { return _map.find(key); }

    iterator get(const Key& key) { return _map.find(key); }

    Value get_value(const Key& key)
    {
        auto res = _map.find(key);
        if (res != not_a_value()) {
            return res->second;
        }
        return _default_value;
    }

    const_iterator not_a_value() { return _map.end(); }

    void set_default(const Value& value) { _default_value = value; }

 protected:
    void _add(const Key& key, const Value& value)
    {
        _map.insert(Pair(key, value));
    }

    DictType _map;
    Value _default_value;
};

}  // namespace detail

template<class T, class U, std::size_t N>
using HashDict = detail::DictBase<etl::unordered_map<T, U, N>>;

template<class T, class U, std::size_t N>
using FlatDict = detail::DictBase<etl::flat_map<T, U, N>>;

}  // namespace memory

}  // namespace utils

#endif /* UTILS_MEMORY_DICT_HPP_ */
