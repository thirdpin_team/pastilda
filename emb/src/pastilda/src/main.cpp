#include <critical.hpp>

#include <app/App.hpp>
#include <config/FreeRTOSConfig.h>
#include <config/HwConfig.h>
#include <hw/Rcc.hpp>
#include <hw/System.hpp>

namespace rtos = cpp_freertos;

inline namespace {

/**
 * Reset RCC before main
 *
 * This class constructor will be called before main().
 * Its propose is to reset RCC and enable it.
 *
 */
class ResetHandler
{
 public:
    using Rcc = hw::Rcc<hw::config::RCC_HSE_25MHZ_TO_HCLK_168MHZ,
                        hw::config::RCC_PERIPHS_FOR_ENABLE,
                        hw::config::RCC_PERIPHS_FOR_RESET>;

    ResetHandler(void)
    {
        Rcc::reset();
        Rcc::clock_setup();
        Rcc::enable();

        hw::system::systick_init();
        hw::system::irq_disable();  // it will be enabled by Scheduler
    }
} static after_reset_handler;
}  // namespace

/**
 * Application entry point
 */
int main()
{
    // Shift vector table for in a boot mode
#ifdef PASTILDA_SHIFTED
#warning "Boot mode enabled! Vectors table reallocated."
    SCB->VTOR = 0x8008000;
#endif

#if (configUSE_TRACE_FACILITY == 1)
//    vTraceEnable(TRC_START);
#endif

    static const app::App app;
    return app.exec();
}

/**
 * Standard operator new overload
 */
void* operator new(size_t sz)
{
    return pvPortMalloc(sz);
}

/**
 * Standard operator new[] overload
 */
void* operator new[](size_t sz)
{
    return pvPortMalloc(sz);
}

/**
 * Standard operator delete overload
 */
void operator delete(void* p)
{
    configASSERT(false);
}

/**
 * Standard operator delete[] overload
 */
void operator delete[](void* p)
{
    configASSERT(false);
}

/**
 * Hook for FreeRTOS assert fail call
 */
void vAssertCalled(const char* pcFile, unsigned long ulLine)
{
    rtos::CriticalSection::Enter();
    {
        while (true) {
            portNOP();
        }
    }
    rtos::CriticalSection::Exit();
}

/**
 * Hook for FreeRTOS malloc fail call
 */
void vApplicationMallocFailedHook()
{
    configASSERT(false);
}

/**
 * Hook for FreeRTOS stack overflow call
 */
extern "C" {
void vApplicationStackOverflowHook(TaskHandle_t, char* name)
{
    configASSERT(false);
}
}