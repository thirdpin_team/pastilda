/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HW_SDCARD_H_
#define HW_SDCARD_H_

#include <libopencm3/stm32/f4/nvic.h>
#include <libopencm3/stm32/sdio.h>

#include <config/HwConfig.h>
#include <utils/patterns/Singleton.hpp>

#include "SdcardDefinitions.h"

namespace hw {

namespace sdcard {

class SdcardDriver : public utils::patterns::Singleton<SdcardDriver>
{
    friend class utils::patterns::Singleton<SdcardDriver>;

    friend void ::sdio_isr();
    friend void ::dma2_stream3_isr();

 public:
    CardInfo sdcard_info;

    const bool& is_initialized;

    OpResult read_block(uint8_t* buffer, uint64_t address);
    OpResult write_block(uint8_t* buffer, uint64_t address);
    OpResult read_blocks(uint8_t* buffer, uint64_t address, uint32_t blockcount);
    OpResult write_blocks(uint8_t* buffer, uint64_t address, uint32_t blockcount);
    OpResult wait_read_operation(void);
    OpResult wait_write_operation(void);
    OpResult erase(uint64_t startaddr, uint64_t endaddr);
    TransferState get_status(void);

    uint8_t detect(void);

 private:
    enum class ExchangeDir
    {
        TRANSMIT,
        RECEIVE
    };

    using WideBusMode = SDIO_WideBusMode;

    bool _is_initialized = false;

    cm3cpp::gpio::Gpio _sd_det;

    SDIO_config _transfer_config;

    SDIO_command _command;
    SDIO_data_config _data_config;

    uint32_t _card_type = sdcard::SDIO_STD_CAPACITY_SD_CARD_V1_1;
    uint32_t _csd_tab[4] = {0};
    uint32_t _cid_tab[4] = {0};
    uint32_t _rca = 0;
    uint8_t _sd_status_tab[16];

    uint32_t _stop_condition = 0;
    volatile OpResult _sd_transfer_result = OpResult::OK;

    volatile bool _is_dma_end_of_transfer = false;
    volatile bool _is_transfer_end = false;

    SdcardDriver();

    OpResult _power_on(void);
    OpResult _power_off(void);
    OpResult _cards_init(void);
    OpResult _get_card_info(CardInfo* info);
    OpResult _select_deselect(uint64_t address);
    OpResult _enable_wide_bus_operation(WideBusMode);
    OpResult _stop_transfer(void);
    OpResult _get_card_status(SdStatusReg* status);
    OpResult _send_sd_status(uint32_t* status);
    OpResult _send_status(uint32_t* status);
    CardState _get_state(void);
    OpResult _cmd_error(void);
    OpResult _cmd_R1_error(uint8_t cmd);
    OpResult _cmd_R7_error(void);
    OpResult _cmd_R3_error(void);
    OpResult _cmd_R2_error(void);
    OpResult _cmd_R6_error(uint8_t cmd, uint16_t* rca);
    OpResult _wide_bus(bool enable);
    OpResult _is_busy(CardState&);
    OpResult _find_SCR(uint16_t rca, uint32_t* scr);

    void _init_sdio_periph();
    OpResult _init_sdcard();

    void _sdio_irq_proc(void);
    void _dma_irq_proc(void);

    void _reconfigure_bus(WideBusMode, BusClockDiv);

    void _setup_dma(uint32_t* buffer, uint32_t size, ExchangeDir);

    void _wait_transfer(uint32_t& timeout);
};

} /* namespace sdcard */

} /* namespace hw */

#endif /* HW_SDCARD_H_ */
