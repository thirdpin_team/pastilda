/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKS_OUTPUTMANAGETASK_H_
#define TASKS_OUTPUTMANAGETASK_H_

#include <new>

#include <strs/StringFixed.hpp>

#include <config/TasksConfig.h>
#include <database/Entry.h>
#include <hw/kbd/Package.hpp>
#include <utils/kbd/AsciiBufferedConverter.hpp>
#include <utils/keepass/AutotypeReader.h>

#include "common/IKahnProcessTask.hpp"
#include "common/Message.hpp"

namespace tasks {

namespace msg {

enum class OutputManagerCommand : uint8_t
{
    PASTILDA_START,
    MESSAGE,
    PASSWORD,
    DB_ERROR_MESSAGE,
    CREDENTIALS,
    END,
};

union OutputManagerData
{
    using StringType =
      strs::StringFixed<config::OutputManagerTask::MAX_MESSAGE_STRING_SIZE>;

 public:
    StringType str;
    db::Entry* db_entry;

    OutputManagerData() { new (&str) StringType(); }

    ~OutputManagerData() { str.~StringType(); }
};

using OutputManagerMessage =
  common::Message<OutputManagerCommand, OutputManagerData>;

} /* namespace msg */

class OutputManagerTask final
  : public common::IKahnProcessTask<config::OutputManagerTask::NAME,
                                    msg::OutputManagerMessage,
                                    config::OutputManagerTask::FIFO_SIZE>
{
 public:
    enum State
    {
        IDLE,
        PASSWORD,
        PASSWORD_FAIL,
        NAVIGATION,
        CLEANING,
        LOGIN,
        END,
    };

    enum StateAction
    {
        STAY,
        NEXT,
        FAIL,
        QUIT,
    };

    OutputManagerTask();
    virtual ~OutputManagerTask() = default;

    void Run();

 private:
    class Model
    {
     public:
        template<class T>
        struct Field : public T
        {
            using T::T;
            using Type = T;
            using Cursor = typename T::iterator;

            Cursor cursor;

            Field& operator=(const Field& other);
            inline void cursor_to_begin() { cursor = this->begin(); }
            inline void cursor_to_end() { cursor = this->end(); }

            inline void reset();
        };

        enum UpdateType
        {
            APPEND,
            GREETING_ONLY,
            REPLACE
        };

        using MainField = Field<
          strs::StringFixed<config::OutputManagerTask::MAX_OUTPUT_SRTING_SIZE>>;
        using GreetingField = Field<strs::StringFixed<32>>;

        GreetingField greeting_field;
        MainField main_field;

        GreetingField greeting_field_old;
        MainField main_field_old;

        Model(const MsgType& fifo)
        {
            _fifo = &fifo;
            greeting_field.cursor_to_begin();
            main_field.cursor_to_begin();
        }

        void reset();
        void update(UpdateType type = REPLACE);

        inline void save();
        inline void revert();

     private:
        const MsgType* _fifo;

        void _update_greeting();
        void _update_main();
    };

    class Printer
    {
     public:
        using KbdBuffer =
          etl::vector<hw::kbd::Package,
                      config::OutputManagerTask::PROCESSING_PCKG_BUFFER_SIZE>;

        using AsciiBufferedConverter =
          utils::kbd::AsciiBufferedConverter<KbdBuffer>;

        using AutotypeReader = utils::keepass::AutotypeReader;

        struct CompareResult
        {
            strs::StringView str_to_print;
            uint32_t remove_length;
            bool is_different = true;
        };

        Printer() : _ascii_buffered_converter(&_kbd_buffer) {}

        void print(const Model* const model);
        void print(const strs::StringView& str);
        void print(const char c);
        void print(db::Entry& db_entry);
        void remove(uint32_t n);

     private:
        void _default_login_print(db::Entry& db_entry);
        void _special_login_print(db::Entry& db_entry);

        CompareResult _compare_strings(const strs::StringView str_new,
                                       const strs::StringView str_old);

        KbdBuffer _kbd_buffer;
        AsciiBufferedConverter _ascii_buffered_converter;
        AutotypeReader _autotype_reader;
    };

    Model _model;
    Printer _printer;

    State _state = State::IDLE;

    void _state_machine_proc();

    StateAction _idle_proc();
    StateAction _password_proc();
    StateAction _password_fail_proc();
    StateAction _navigation_proc();
    StateAction _cleaning_proc();
    StateAction _login_proc();
    StateAction _end_proc();
};

} /* namespace tasks */

#endif /* TASKS_OUTPUTMANAGETASK_H_ */
