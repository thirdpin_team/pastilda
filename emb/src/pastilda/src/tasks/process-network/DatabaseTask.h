/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKS_DATABASETASK_H_
#define TASKS_DATABASETASK_H_

#include <strs/StringView.hpp>

#include <config/TasksConfig.h>
#include <database/Entry.h>
#include <tree/XmlToTreeBuilder.hpp>
#include <utils/keepass/KeepassReader.h>
#include <utils/memory/PrimitivePool.hpp>

#include "common/IKahnProcessTask.hpp"
#include "common/Message.hpp"

namespace tasks {

namespace detail {
enum class DbState
{
    NOT_A_STATE =
      utils::keepass::KeepassReader::DecryptionResult::NONE,  // default value
    DB_OK,
    INVALID_DB_VERSION,
    INVALID_DB_PASSWORD,
    NO_DB_CREDENTIALS_SELECTED,
    CAN_NOT_OPEN_DB_FILE,
    DB_FILE_HASH_IS_CORRUPTED,
    DB_FILE_IS_CLOSED,
    DB_FILE_FORBIDDEN_SYMBOLS
};
}  // namespace detail

namespace _ = detail;

namespace msg {

enum DatabaseCommand : uint8_t
{
    DECRYPT,
    DB_CLOSE,
    DECRYPT_PASS,
};

using DatabasePassphrase = strs::StringView;
using DatabaseMessage = common::Message<DatabaseCommand, DatabasePassphrase>;

}  // namespace msg

class DatabaseTask final
  : public common::IKahnProcessTask<config::DatabaseTask::NAME,
                                    msg::DatabaseMessage,
                                    config::DatabaseTask::FIFO_SIZE>
{
 public:
    DatabaseTask();
    virtual ~DatabaseTask() = default;

    void Run();

 private:
    enum State
    {
        IDLE,
        DECRYPT,
        OPENED,
    };

    enum StateAction
    {
        STAY,
        NEXT,
        FAIL,
        QUIT,
    };

    class DatabaseManager
    {
     public:
        using Tree = tree::TreeImpl;
        using Pool =
          utils::memory::PrimitivePool<char,
                                       config::DatabaseTask::STRING_POOL_SIZE>;

        using Builder = tree::XmlToTreeBuilder<Tree, Pool>;

        DatabaseManager(const MsgType& fifo);
        ~DatabaseManager() = default;

        void build_database();
        void close_database();

        db::Entry& get_current_entry();
        inline _::DbState get_database_state();
        inline bool is_database_ok() { return _is_db_ok; }

     private:
        class Decrypter
        {
         public:
            using KeepassReader = utils::keepass::KeepassReader;
            using Password = KeepassReader::Password;
            using ProcBuffer =
              etl::array<uint8_t, KeepassReader::DECRYPTED_BLOCK_SIZE>;

            Decrypter(const MsgType& fifo, Builder&& tree_builder);
            ~Decrypter() = default;

            inline void set_database(const char* db_name)
            {
                _db_name = db_name;
            }

            void verify_db_password();
            void decrypt();
            void decrypt_entry_password(Tree::iterator start,
                                        const Tree::Node* current_node);

            Password* get_entry_password();

            inline bool is_error() { return _is_error; }
            inline _::DbState get_db_state() { return _db_state; }

         private:
            const char* _db_name;
            const MsgType* _fifo;

            KeepassReader _keepass_reader;
            Password _entry_password;
            Builder _tree_builder;

            _::DbState _db_state;
            bool _is_error;

            ProcBuffer _processing_buffer;

            inline void _set_db_password();
            inline void _error_process();
        };

        bool _is_db_ok;
        Pool _string_pool;
        Tree* _tree;
        Decrypter _decrypter;
        db::Entry _decrypted_entry;
    };

    DatabaseManager _db_manager;
    State _state = State::IDLE;

    void _state_machine_proc();

    StateAction _idle_proc();
    StateAction _decrypt_proc();
    StateAction _opened_proc();

    void _send_fail();
};

} /* namespace tasks */

#endif /* TASKS_DATABASETASK_H_ */
