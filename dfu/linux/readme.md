# Linux instruction

## Files description

* **readme.md**       — this manual
* **start.sh**        — script for starting flashing device
* **boot.dfu**        — the latest release of a bootloader (you should place it here)
* **flash_dev.py**    — main script for flashing
* **dfu-convert**     — script for converting _dfu_ to _bin_ format and back

## Before starting the script

Script require Python 3 installed on your system. Also it has to be istalled ItelHex python package. To install IntelHex package use a command (with root privileges, if it needed):

```
pip install ItelHex
```

The main util using for flashing is _dfu-util_. You have to install _dfu-util_ package on your system. For example, if you have Ubuntu OS you can use a command like:

```
sudo apt-get install dfu-util
```

Also you have to create a file like /etc/udev/rules.d/10-dfu-util.rules to avoid permission problems. Put there

```
ATTRS{idProduct}=="df11", ATTRS{idVendor}=="0483", MODE="664", GROUP="plugdev"
```

## How to start flashing process

If your device have no pre-loaded bootloader you have to go through the next steps:

* Copy files from dfu/sdcard folder to your SD card and insert it to the Pastilda.
* Set read only attribute for file _pastilda.srec_. That is necessary to forbid bootloader removing _pastilla.srec_ file from SD card during testing stage. You can use command:

```
chmod -w pastilda.srec
```

* Find "boot" label on your Pastilda. Then find two metal holes where (these are Boot0 and VDD). You have to connect them to each other for Pastilda enters a DFU mode.
* Insert your Pastilda to the PC. After that you will see new 'STM Device in DFU Mode' by typing `lsusb` in your favorite terminal.
* After that you can disconnect Boot0 and VDD pins.
* Run _start.sh_ from dfu folder by typing `sh start.sh` in your terminal and pressing _Enter_. It will start the script and load bootloader to your device.
* Once bootloader is loaded it will find pastilda.srec file on your SD card and will start to load firmware.
* After programming is finished then reset device. LEDs on your Pastilda start to blink with 1s period.
* New Keyboard and Mass Storage Device will appear in your system.
* That's it! Now you can connect your keyboard to Pastilda and try it's functionality.
* Check if pastilda works by typing something by the keyboard connected to pastilda.
