/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "KbdBufferedConverter.h"

namespace utils {

namespace kbd {

using KbdKey = hw::kbd::Key;

KbdBufferedConverter::KbdBufferedConverter() : _input_package(nullptr)
{
    _reset_input();
}

strs::StringView KbdBufferedConverter::get_line()
{
    auto start = _buffer_pos;
    _buffer_pos = _ascii_buffer.end();
    return strs::StringView(start, _ascii_buffer.end() - start);
}

void KbdBufferedConverter::process(const hw::kbd::Package& package)
{
    _input_package = &package;

    if (package.keys_count() != 0) {
        if (_last_package == hw::kbd::ZERO_PACKAGE) {
            _store_input_package();
        }
        else if (not _is_series_package_too_big()) {
            _store_input_package_series();
        }
    }
    else {
        _same_keys = 0;
        if (_buffer_pos == _ascii_buffer.end()) {
            _reset_input();
        }
    }

    _last_package = package;
}

bool KbdBufferedConverter::_is_series_package_too_big()
{
    return (_input_package->keys_count() == 5);
}

void KbdBufferedConverter::_store_input_package()
{
    auto& special_keys = _input_package->special;

    for (auto raw_key : _input_package->keys) {
        if (raw_key == KbdKey::NOT_A_KEY) {
            break;
        }
        kbd::KeyObj key(raw_key, special_keys);
        _store_key(key);
    }
}

void KbdBufferedConverter::_store_input_package_series()
{
    // Check if we have enough number of same keys to start print them out
    if (_same_keys >= MAX_SAME_KEYS_COUNT_BEFORE_OUTPUT) {
        _store_key(KeyObj(_input_package->keys[0], _input_package->special));
        return;
    }

    // Renew the set of keys with keys from the last package
    _last_keys_set.clear();
    for (auto& key : _last_package.keys) {
        if (key == KbdKey::NOT_A_KEY) {
            break;
        }
        _last_keys_set.insert(key);
    }

    // If one of the key not in set, then save it as a new key
    KbdKey new_key = KbdKey::NOT_A_KEY;
    for (auto& key : _input_package->keys) {
        if (key == KbdKey::NOT_A_KEY) {
            break;
        }

        if (not _last_keys_set.is_include(key)) {
            new_key = key;
        }
    }

    // If we have a new key then store it
    if (new_key != KbdKey::NOT_A_KEY) {
        _store_key(KeyObj(new_key, _input_package->special));
    }
    // Else check if _input_package is single key
    // in the package and same as last
    else {
        if (_input_package->keys_count() == 1) {
            _same_keys += 1;
        }
    }
}

void KbdBufferedConverter::_store_key(const kbd::KeyObj& key)
{
    if (key.get_kbd_key() != KbdKey::NOT_A_KEY) {
        if (not _ascii_buffer.full()) {
            _ascii_buffer.push_back(key.get_ascii_code());
        }
    }
}

void KbdBufferedConverter::_reset_input()
{
    _ascii_buffer.clear();
    _buffer_pos = _ascii_buffer.begin();
    _last_package = hw::kbd::ZERO_PACKAGE;
}

} /* namespace kbd */

} /* namespace utils */
