/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_MEMORY_PRIMITIVE_HPP_
#define UTILS_MEMORY_PRIMITIVE_HPP_

namespace utils {

namespace memory {

template<typename T, std::size_t N>
class PrimitivePool
{
 public:
    using value_type = T;
    using pointer = T*;
    using const_pointer = const T*;
    using reference = T&;
    using const_reference = const T&;
    using iterator = pointer;
    using const_iterator = const_pointer;

    static constexpr std::size_t SIZE = N;

    struct MemoryChunk
    {
        pointer start;
        pointer end;
    };

    PrimitivePool() : _index(0)
    {
        static_assert(std::is_fundamental<value_type>::value,
                      "Not a primitive type!");
    };

    ~PrimitivePool() = default;

    value_type* allocate()
    {
        if (_index + 1 <= SIZE) {
            ++_index;
            return &_buffer[_index - 1];
        }
        return nullptr;
    }

    MemoryChunk allocate_n(std::size_t n)
    {
        MemoryChunk chunk = {nullptr, nullptr};

        if (_index + n <= SIZE) {
            chunk.start = &_buffer[_index];
            _index += n;
            chunk.end = &_buffer[_index - 1];

            return chunk;
        }
        return chunk;
    }

    iterator begin() { return &_buffer[0]; }

    iterator end() { return &_buffer[_index]; }

    std::size_t size() { return _index; }

    std::size_t full() { return _index == SIZE; }

    std::size_t empty() { return _index == 0; }

    void release_all() { _index = 0; }

 private:
    PrimitivePool(const PrimitivePool&);
    PrimitivePool& operator=(const PrimitivePool&);

    std::size_t _index;
    T _buffer[SIZE];
};

}  // namespace memory

}  // namespace utils

#endif /* UTILS_MEMORY_PRIMITIVE_HPP_ */
