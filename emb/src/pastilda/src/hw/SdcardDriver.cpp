/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits>

#include "SdcardDriver.h"

namespace {

template<typename EnumT, typename UnderlyingT = std::underlying_type_t<EnumT>>
UnderlyingT to_number(EnumT value)
{
    return static_cast<UnderlyingT>(value);
}

}  // namespace


namespace hw {

namespace sdcard {

SdcardDriver::SdcardDriver() :
  is_initialized(_is_initialized),
  _transfer_config{.polarity = SDIO_CK_RISING_EDGE,
                   .bypass = false,
                   .power_save = false,
                   .bus_wide = WIDE_BUS_1,
                   .flow_control = false,
                   .clock_divider = to_number(BusClockDiv::INIT)}
{
    _init_sdio_periph();
    _is_initialized = (_init_sdcard() == OpResult::OK);
}

sdcard::OpResult SdcardDriver::_init_sdcard()
{
    auto is_fail = [](OpResult r) { return r != OpResult::OK; };

    sdio_deinit();
    sdio_config(&_transfer_config);  // apply default config

    _reconfigure_bus(WideBusMode::WIDE_BUS_1, BusClockDiv::INIT);
    if (auto r = _power_on(); is_fail(r)) {
        return r;
    }

    if (auto r = _cards_init(); is_fail(r)) {
        return r;
    }

    _reconfigure_bus(WideBusMode::WIDE_BUS_1, BusClockDiv::TRANSFER);
    if (auto r = _get_card_info(&sdcard_info); is_fail(r)) {
        return r;
    }

    if (auto r = _select_deselect((uint32_t)(sdcard_info.rca << 16)); is_fail(r)) {
        return r;
    }

    return _enable_wide_bus_operation(WideBusMode::WIDE_BUS_4);
}

sdcard::OpResult SdcardDriver::read_block(uint8_t* buffer, uint64_t address)
{
    if (_card_type == sdcard::SDIO_HIGH_CAPACITY_SD_CARD) {
        address /= 512;
    }

    if (address > uint64_t(std::numeric_limits<uint32_t>::max())) {
        // Address size can not be > 32bit
        return OpResult::ADDR_OUT_OF_RANGE;
    }

    auto result = OpResult::OK;

    _sd_transfer_result = OpResult::OK;
    _is_transfer_end = false;
    _stop_condition = 0;

    SDIO_DCTRL = 0x0;

    _setup_dma((uint32_t*)buffer, sdcard::BLOCK_SIZE, ExchangeDir::RECEIVE);

    _command.index = sdcard::SD_CMD_SET_BLOCKLEN;
    _command.arg = sdcard::BLOCK_SIZE;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCKLEN);

    if (result != OpResult::OK)
        return result;

    _data_config.direction = CARD_TO_CONTROLLER;
    _data_config.period = sdcard::SD_DATATIMEOUT;
    _data_config.size = BLOCK_LENGTH_512_B;
    _data_config.length = sdcard::BLOCK_SIZE;
    _data_config.mode = BLOCK_DATA_TRANSFER;
    _data_config.enable = true;
    sdio_data_config(&_data_config);

    _command.index = sdcard::SD_CMD_READ_SINGLE_BLOCK;
    _command.arg = (uint32_t)address;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_READ_SINGLE_BLOCK);

    if (result != OpResult::OK)
        return result;

    return (result);
}

sdcard::OpResult SdcardDriver::write_block(uint8_t* buffer, uint64_t address)
{
    if (_card_type == sdcard::SDIO_HIGH_CAPACITY_SD_CARD) {
        address /= 512;
    }

    if (address > uint64_t(std::numeric_limits<uint32_t>::max())) {
        // Address size can not be > 32bit
        return OpResult::ADDR_OUT_OF_RANGE;
    }

    auto result = OpResult::OK;

    _sd_transfer_result = OpResult::OK;
    _is_transfer_end = false;
    _stop_condition = 0;

    SDIO_DCTRL = 0x0;

    _setup_dma((uint32_t*)buffer, sdcard::BLOCK_SIZE, ExchangeDir::TRANSMIT);

    _command.index = sdcard::SD_CMD_SET_BLOCKLEN;
    _command.arg = sdcard::BLOCK_SIZE;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCKLEN);

    if (result != OpResult::OK)
        return (result);

    _command.index = sdcard::SD_CMD_WRITE_SINGLE_BLOCK;
    _command.arg = uint32_t(address);
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_WRITE_SINGLE_BLOCK);

    if (result != OpResult::OK)
        return (result);

    _data_config.direction = CONTROLLER_TO_CARD;
    _data_config.period = sdcard::SD_DATATIMEOUT;
    _data_config.size = BLOCK_LENGTH_512_B;
    _data_config.length = sdcard::BLOCK_SIZE;
    _data_config.mode = BLOCK_DATA_TRANSFER;
    _data_config.enable = true;
    sdio_data_config(&_data_config);

    return (result);
}

sdcard::OpResult SdcardDriver::read_blocks(uint8_t* buffer,
                                           uint64_t address,
                                           uint32_t blockcount)
{
    if (_card_type == sdcard::SDIO_HIGH_CAPACITY_SD_CARD) {
        address /= 512;
    }

    if (address > uint64_t(std::numeric_limits<uint32_t>::max())) {
        // Address size can not be > 32bit
        return OpResult::ADDR_OUT_OF_RANGE;
    }

    sdcard::OpResult result = OpResult::OK;
    _sd_transfer_result = OpResult::OK;
    _is_transfer_end = false;
    _stop_condition = 1;

    SDIO_DCTRL = 0x0;

    _setup_dma((uint32_t*)buffer, sdcard::BLOCK_SIZE * blockcount, ExchangeDir::RECEIVE);

    _command.index = sdcard::SD_CMD_SET_BLOCKLEN;
    _command.arg = sdcard::BLOCK_SIZE;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCKLEN);

    if (result != OpResult::OK)
        return result;

    _data_config.direction = CARD_TO_CONTROLLER;
    _data_config.period = sdcard::SD_DATATIMEOUT;
    _data_config.size = BLOCK_LENGTH_512_B;
    _data_config.length = (sdcard::BLOCK_SIZE * blockcount);
    _data_config.mode = BLOCK_DATA_TRANSFER;
    _data_config.enable = true;
    sdio_data_config(&_data_config);

    _command.index = sdcard::SD_CMD_READ_MULT_BLOCK;
    _command.arg = (uint32_t)address;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_READ_MULT_BLOCK);

    if (result != OpResult::OK)
        return result;

    return (result);
}

sdcard::OpResult SdcardDriver::write_blocks(uint8_t* buffer,
                                            uint64_t address,
                                            uint32_t blockcount)
{
    if (_card_type == sdcard::SDIO_HIGH_CAPACITY_SD_CARD) {
        address /= 512;
    }

    if (address > uint64_t(std::numeric_limits<uint32_t>::max())) {
        // Address size can not be > 32bit
        return OpResult::ADDR_OUT_OF_RANGE;
    }

    auto result = OpResult::OK;

    _sd_transfer_result = OpResult::OK;
    _is_transfer_end = false;
    _stop_condition = 1;

    SDIO_DCTRL = 0x0;

    _setup_dma((uint32_t*)buffer, sdcard::BLOCK_SIZE * blockcount, ExchangeDir::TRANSMIT);

    _command.index = sdcard::SD_CMD_SET_BLOCKLEN;
    _command.arg = sdcard::BLOCK_SIZE;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCKLEN);

    if (result != OpResult::OK)
        return (result);

    _command.index = sdcard::SD_CMD_APP_CMD;
    _command.arg = (uint32_t)(uint32_t)(_rca << 16);
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

    if (result != OpResult::OK)
        return (result);

    _command.index = sdcard::SD_CMD_SET_BLOCK_COUNT;
    _command.arg = blockcount;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCK_COUNT);

    if (result != OpResult::OK)
        return (result);

    _command.index = sdcard::SD_CMD_WRITE_MULT_BLOCK;
    _command.arg = (uint32_t)address;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_WRITE_MULT_BLOCK);

    if (result != OpResult::OK)
        return (result);

    _data_config.direction = CONTROLLER_TO_CARD;
    _data_config.period = sdcard::SD_DATATIMEOUT;
    _data_config.size = BLOCK_LENGTH_512_B;
    _data_config.length = (sdcard::BLOCK_SIZE * blockcount);
    _data_config.mode = BLOCK_DATA_TRANSFER;
    _data_config.enable = true;
    sdio_data_config(&_data_config);

    return (result);
}

sdcard::OpResult SdcardDriver::wait_read_operation(void)
{
    auto result = OpResult::OK;
    uint32_t timeout = sdcard::SD_DATATIMEOUT;

    _wait_transfer(timeout);

    _is_dma_end_of_transfer = false;
    timeout = sdcard::SD_DATATIMEOUT;

    while (((SDIO_STA & SDIO_RXACT)) && (timeout > 0)) {
        timeout--;
    }

    if (_stop_condition == 1) {
        result = _stop_transfer();
        _stop_condition = 0;
    }

    if ((timeout == 0) && (result == OpResult::OK)) {
        result = OpResult::DATA_TIMEOUT;
    }

    sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);

    if (_sd_transfer_result != OpResult::OK) {
        return (_sd_transfer_result);
    }
    else {
        return (result);
    }
}

sdcard::OpResult SdcardDriver::wait_write_operation(void)
{
    sdcard::OpResult result = OpResult::OK;
    uint32_t timeout = sdcard::SD_DATATIMEOUT;

    _wait_transfer(timeout);

    _is_dma_end_of_transfer = false;
    timeout = sdcard::SD_DATATIMEOUT;

    while (((SDIO_STA & SDIO_TXACT)) && (timeout > 0)) {
        timeout--;
    }

    if (_stop_condition == 1) {
        result = _stop_transfer();
        _stop_condition = 0;
    }

    if ((timeout == 0) && (result == OpResult::OK)) {
        result = OpResult::DATA_TIMEOUT;
    }

    sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);

    if (_sd_transfer_result != OpResult::OK) {
        return (_sd_transfer_result);
    }
    else {
        return (result);
    }
}

sdcard::OpResult SdcardDriver::erase(uint64_t startaddr, uint64_t endaddr)
{
    sdcard::OpResult result = OpResult::OK;
    uint32_t delay = 0;
    uint32_t maxdelay = 0;
    CardState cardstate = CardState::ERROR;

    if (((_csd_tab[1] >> 20) & sdcard::SD_CCCC_ERASE) == 0) {
        result = OpResult::REQUEST_NOT_APPLICABLE;
        return (result);
    }

    maxdelay = 120000 / (_transfer_config.clock_divider + 2);

    if (sdio_get_card_status_1() & sdcard::SD_CARD_LOCKED) {
        result = OpResult::LOCK_UNLOCK_FAILED;
        return (result);
    }

    if (_card_type == sdcard::SDIO_HIGH_CAPACITY_SD_CARD) {
        startaddr /= 512;
        endaddr /= 512;
    }

    if ((sdcard::SDIO_STD_CAPACITY_SD_CARD_V1_1 == _card_type) ||
        (sdcard::SDIO_STD_CAPACITY_SD_CARD_V2_0 == _card_type) ||
        (sdcard::SDIO_HIGH_CAPACITY_SD_CARD == _card_type)) {
        _command.index = sdcard::SD_CMD_SD_ERASE_GRP_START;
        _command.arg = startaddr;
        _command.response = RESPONSE_SHORT;
        _command.wait = WAIT_NO;
        _command.enable_CPSM = true;
        sdio_send_command(&_command);

        result = _cmd_R1_error(sdcard::SD_CMD_SD_ERASE_GRP_START);

        if (result != OpResult::OK)
            return (result);

        _command.index = sdcard::SD_CMD_SD_ERASE_GRP_END;
        _command.arg = endaddr;
        _command.response = RESPONSE_SHORT;
        _command.wait = WAIT_NO;
        _command.enable_CPSM = true;
        sdio_send_command(&_command);

        result = _cmd_R1_error(sdcard::SD_CMD_SD_ERASE_GRP_END);

        if (result != OpResult::OK)
            return (result);
    }

    _command.index = sdcard::SD_CMD_ERASE;
    _command.arg = 0;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_ERASE);

    if (result != OpResult::OK)
        return (result);

    for (delay = 0; delay < maxdelay; delay++) {}

    result = _is_busy(cardstate);
    delay = sdcard::SD_DATATIMEOUT;

    while (
      (delay > 0) && (result == OpResult::OK) &&
      ((cardstate == CardState::PROGRAMMING) || (cardstate == CardState::RECEIVING))) {
        result = _is_busy(cardstate);
        delay--;
    }

    return (result);
}

sdcard::TransferState SdcardDriver::get_status(void)
{
    auto cardstate = CardState::TRANSFER;
    cardstate = _get_state();

    if (cardstate == CardState::TRANSFER) {
        return (TransferState::OK);
    }
    else if (cardstate == CardState::ERROR) {
        return (TransferState::ERROR);
    }
    else {
        return (TransferState::BUSY);
    }
}

uint8_t SdcardDriver::detect(void)
{
    uint8_t status = sdcard::SD_PRESENT;

    if (_sd_det.get() != 0) {
        status = sdcard::SD_NOT_PRESENT;
    }

    return status;
}

void SdcardDriver::_sdio_irq_proc(void)
{
    if (sdio_get_flag_status(SDIO_DATAEND)) {
        _sd_transfer_result = OpResult::OK;
        sdio_clear_flag_status(SDIO_DATAEND);
        _is_transfer_end = true;
    }
    else if (sdio_get_flag_status(SDIO_DCRCFAIL)) {
        sdio_clear_flag_status(SDIO_DCRCFAIL);
        _sd_transfer_result = OpResult::DATA_CRC_FAIL;
    }
    else if (sdio_get_flag_status(SDIO_DTIMEOUT)) {
        sdio_clear_flag_status(SDIO_DTIMEOUT);
        _sd_transfer_result = OpResult::DATA_TIMEOUT;
    }
    else if (sdio_get_flag_status(SDIO_RXOVERR)) {
        sdio_clear_flag_status(SDIO_RXOVERR);
        _sd_transfer_result = OpResult::RX_OVERRUN;
    }
    else if (sdio_get_flag_status(SDIO_TXUNDERR)) {
        sdio_clear_flag_status(SDIO_TXUNDERR);
        _sd_transfer_result = OpResult::TX_UNDERRUN;
    }
    else if (sdio_get_flag_status(SDIO_STBITERR)) {
        sdio_clear_flag_status(SDIO_STBITERR);
        _sd_transfer_result = OpResult::START_BIT_ERR;
    }

    sdio_disable_interrupt(SDIO_DCRCFAIL | SDIO_DTIMEOUT | SDIO_DATAEND | SDIO_TXFIFOHE |
                           SDIO_RXFIFOHF | SDIO_TXUNDERR | SDIO_RXOVERR | SDIO_STBITERR);
}

void SdcardDriver::_dma_irq_proc(void)
{
    auto const DMA = config::sdio::dma::PERIPH;
    auto const DMA_STREAM = config::sdio::dma::STREAM;

    if (dma_get_interrupt_flag(DMA, DMA_STREAM, DMA_HISR_TCIF4)) {
        _is_dma_end_of_transfer = true;
        dma_clear_interrupt_flags(DMA, DMA_STREAM, (DMA_HISR_TCIF4 | DMA_HISR_FEIF4));
    }
}

sdcard::OpResult SdcardDriver::_power_on(void)
{
    auto result = OpResult::OK;
    uint32_t response = 0, count = 0, validvoltage = 0;
    uint32_t SDType = sdcard::SD_STD_CAPACITY;

    sdio_enable();
    sdio_ck_enable();

    _command.index = sdcard::SD_CMD_GO_IDLE_STATE;
    _command.arg = 0;
    _command.response = RESPONSE_NO;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_error();

    if (result != OpResult::OK)
        return result;

    _command.index = sdcard::SDIO_SEND_IF_COND;
    _command.arg = sdcard::SD_CHECK_PATTERN;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R7_error();

    if (result == OpResult::OK) {
        _card_type = sdcard::SDIO_STD_CAPACITY_SD_CARD_V2_0;
        SDType = sdcard::SD_HIGH_CAPACITY;
    }

    else {
        _command.index = sdcard::SD_CMD_APP_CMD;
        _command.arg = 0;
        _command.response = RESPONSE_SHORT;
        _command.wait = WAIT_NO;
        _command.enable_CPSM = true;
        sdio_send_command(&_command);

        result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);
    }

    _command.index = sdcard::SD_CMD_APP_CMD;
    _command.arg = 0;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

    if (result == OpResult::OK) {
        while ((!validvoltage) && (count < sdcard::SD_MAX_VOLT_TRIAL)) {
            _command.index = sdcard::SD_CMD_APP_CMD;
            _command.arg = 0;
            _command.response = RESPONSE_SHORT;
            _command.wait = WAIT_NO;
            _command.enable_CPSM = true;
            sdio_send_command(&_command);

            result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

            if (result != OpResult::OK)
                return result;

            _command.index = sdcard::SD_CMD_SD_APP_OP_COND;
            _command.arg = (sdcard::SD_VOLTAGE_WINDOW_SD | SDType);
            _command.response = RESPONSE_SHORT;
            _command.wait = WAIT_NO;
            _command.enable_CPSM = true;
            sdio_send_command(&_command);

            result = _cmd_R3_error();

            if (result != OpResult::OK)
                return result;

            response = sdio_get_card_status_1();
            validvoltage = (((response >> 31) == 1) ? 1 : 0);
            count++;
        }

        if (count >= sdcard::SD_MAX_VOLT_TRIAL) {
            result = OpResult::INVALID_VOLTRANGE;
            return (result);
        }

        if (response &= sdcard::SD_HIGH_CAPACITY) {
            _card_type = sdcard::SDIO_HIGH_CAPACITY_SD_CARD;
        }
    }

    return (result);
}

sdcard::OpResult SdcardDriver::_power_off(void)
{
    sdio_disable();
    return OpResult::OK;
}

sdcard::OpResult SdcardDriver::_cards_init(void)
{
    sdcard::OpResult result = OpResult::OK;
    uint16_t rca = 0x01;

    if (sdcard::SDIO_SECURE_DIGITAL_IO_CARD != _card_type) {
        _command.index = sdcard::SD_CMD_ALL_SEND_CID;
        _command.arg = 0;
        _command.response = RESPONSE_LONG;
        _command.wait = WAIT_NO;
        _command.enable_CPSM = true;
        sdio_send_command(&_command);

        result = _cmd_R2_error();

        if (result != OpResult::OK)
            return result;

        _cid_tab[0] = sdio_get_card_status_1();
        _cid_tab[1] = sdio_get_card_status_2();
        _cid_tab[2] = sdio_get_card_status_3();
        _cid_tab[3] = sdio_get_card_status_4();
    }

    if ((sdcard::SDIO_STD_CAPACITY_SD_CARD_V1_1 == _card_type) ||
        (sdcard::SDIO_STD_CAPACITY_SD_CARD_V2_0 == _card_type) ||
        (sdcard::SDIO_SECURE_DIGITAL_IO_COMBO_CARD == _card_type) ||
        (sdcard::SDIO_HIGH_CAPACITY_SD_CARD == _card_type)) {
        _command.index = sdcard::SD_CMD_SET_REL_ADDR;
        _command.arg = 0;
        _command.response = RESPONSE_SHORT;
        _command.wait = WAIT_NO;
        _command.enable_CPSM = true;
        sdio_send_command(&_command);

        result = _cmd_R6_error(sdcard::SD_CMD_SET_REL_ADDR, &rca);

        if (result != OpResult::OK)
            return result;
    }

    if (sdcard::SDIO_SECURE_DIGITAL_IO_CARD != _card_type) {
        _rca = rca;

        _command.index = sdcard::SD_CMD_SEND_CSD;
        _command.arg = (uint32_t)(rca << 16);
        _command.response = RESPONSE_LONG;
        _command.wait = WAIT_NO;
        _command.enable_CPSM = true;
        sdio_send_command(&_command);

        result = _cmd_R2_error();

        if (result != OpResult::OK)
            return result;

        _csd_tab[0] = sdio_get_card_status_1();
        _csd_tab[1] = sdio_get_card_status_2();
        _csd_tab[2] = sdio_get_card_status_3();
        _csd_tab[3] = sdio_get_card_status_4();
    }

    result = OpResult::OK;
    return (result);
}

sdcard::OpResult SdcardDriver::_get_card_info(sdcard::CardInfo* cardinfo)
{
    sdcard::OpResult result = OpResult::OK;
    uint8_t tmp = 0;

    cardinfo->card_type = (uint8_t)_card_type;
    cardinfo->rca = (uint16_t)_rca;

    /*!< Byte 0 */
    tmp = (uint8_t)((_csd_tab[0] & 0xFF000000) >> 24);
    cardinfo->csd.CSDStruct = (tmp & 0xC0) >> 6;
    cardinfo->csd.SysSpecVersion = (tmp & 0x3C) >> 2;
    cardinfo->csd.Reserved1 = tmp & 0x03;

    /*!< Byte 1 */
    tmp = (uint8_t)((_csd_tab[0] & 0x00FF0000) >> 16);
    cardinfo->csd.TAAC = tmp;

    /*!< Byte 2 */
    tmp = (uint8_t)((_csd_tab[0] & 0x0000FF00) >> 8);
    cardinfo->csd.NSAC = tmp;

    /*!< Byte 3 */
    tmp = (uint8_t)(_csd_tab[0] & 0x000000FF);
    cardinfo->csd.MaxBusClkFrec = tmp;

    /*!< Byte 4 */
    tmp = (uint8_t)((_csd_tab[1] & 0xFF000000) >> 24);
    cardinfo->csd.CardComdClasses = tmp << 4;

    /*!< Byte 5 */
    tmp = (uint8_t)((_csd_tab[1] & 0x00FF0000) >> 16);
    cardinfo->csd.CardComdClasses |= (tmp & 0xF0) >> 4;
    cardinfo->csd.RdBlockLen = tmp & 0x0F;

    /*!< Byte 6 */
    tmp = (uint8_t)((_csd_tab[1] & 0x0000FF00) >> 8);
    cardinfo->csd.PartBlockRead = (tmp & 0x80) >> 7;
    cardinfo->csd.WrBlockMisalign = (tmp & 0x40) >> 6;
    cardinfo->csd.RdBlockMisalign = (tmp & 0x20) >> 5;
    cardinfo->csd.DSRImpl = (tmp & 0x10) >> 4;
    cardinfo->csd.Reserved2 = 0; /*!< Reserved */

    if ((_card_type == sdcard::SDIO_STD_CAPACITY_SD_CARD_V1_1) ||
        (_card_type == sdcard::SDIO_STD_CAPACITY_SD_CARD_V2_0)) {
        cardinfo->csd.DeviceSize = (tmp & 0x03) << 10;

        /*!< Byte 7 */
        tmp = (uint8_t)(_csd_tab[1] & 0x000000FF);
        cardinfo->csd.DeviceSize |= (tmp) << 2;

        /*!< Byte 8 */
        tmp = (uint8_t)((_csd_tab[2] & 0xFF000000) >> 24);
        cardinfo->csd.DeviceSize |= (tmp & 0xC0) >> 6;

        cardinfo->csd.MaxRdCurrentVDDMin = (tmp & 0x38) >> 3;
        cardinfo->csd.MaxRdCurrentVDDMax = (tmp & 0x07);

        /*!< Byte 9 */
        tmp = (uint8_t)((_csd_tab[2] & 0x00FF0000) >> 16);
        cardinfo->csd.MaxWrCurrentVDDMin = (tmp & 0xE0) >> 5;
        cardinfo->csd.MaxWrCurrentVDDMax = (tmp & 0x1C) >> 2;
        cardinfo->csd.DeviceSizeMul = (tmp & 0x03) << 1;

        /*!< Byte 10 */
        tmp = (uint8_t)((_csd_tab[2] & 0x0000FF00) >> 8);
        cardinfo->csd.DeviceSizeMul |= (tmp & 0x80) >> 7;

        cardinfo->capacity = (cardinfo->csd.DeviceSize + 1);
        cardinfo->capacity *= (1 << (cardinfo->csd.DeviceSizeMul + 2));
        cardinfo->block_size = 1 << (cardinfo->csd.RdBlockLen);
        cardinfo->capacity *= cardinfo->block_size;
    }
    else if (_card_type == sdcard::SDIO_HIGH_CAPACITY_SD_CARD) {
        /*!< Byte 7 */
        tmp = (uint8_t)(_csd_tab[1] & 0x000000FF);
        cardinfo->csd.DeviceSize = (tmp & 0x3F) << 16;

        /*!< Byte 8 */
        tmp = (uint8_t)((_csd_tab[2] & 0xFF000000) >> 24);

        cardinfo->csd.DeviceSize |= (tmp << 8);

        /*!< Byte 9 */
        tmp = (uint8_t)((_csd_tab[2] & 0x00FF0000) >> 16);

        cardinfo->csd.DeviceSize |= (tmp);

        /*!< Byte 10 */
        tmp = (uint8_t)((_csd_tab[2] & 0x0000FF00) >> 8);

        cardinfo->capacity = ((uint64_t)cardinfo->csd.DeviceSize + 1) * 512 * 1024;
        cardinfo->block_size = 512;
    }

    cardinfo->csd.EraseGrSize = (tmp & 0x40) >> 6;
    cardinfo->csd.EraseGrMul = (tmp & 0x3F) << 1;

    /*!< Byte 11 */
    tmp = (uint8_t)(_csd_tab[2] & 0x000000FF);
    cardinfo->csd.EraseGrMul |= (tmp & 0x80) >> 7;
    cardinfo->csd.WrProtectGrSize = (tmp & 0x7F);

    /*!< Byte 12 */
    tmp = (uint8_t)((_csd_tab[3] & 0xFF000000) >> 24);
    cardinfo->csd.WrProtectGrEnable = (tmp & 0x80) >> 7;
    cardinfo->csd.ManDeflECC = (tmp & 0x60) >> 5;
    cardinfo->csd.WrSpeedFact = (tmp & 0x1C) >> 2;
    cardinfo->csd.MaxWrBlockLen = (tmp & 0x03) << 2;

    /*!< Byte 13 */
    tmp = (uint8_t)((_csd_tab[3] & 0x00FF0000) >> 16);
    cardinfo->csd.MaxWrBlockLen |= (tmp & 0xC0) >> 6;
    cardinfo->csd.WriteBlockPaPartial = (tmp & 0x20) >> 5;
    cardinfo->csd.Reserved3 = 0;
    cardinfo->csd.ContentProtectAppli = (tmp & 0x01);

    /*!< Byte 14 */
    tmp = (uint8_t)((_csd_tab[3] & 0x0000FF00) >> 8);
    cardinfo->csd.FileFormatGrouop = (tmp & 0x80) >> 7;
    cardinfo->csd.CopyFlag = (tmp & 0x40) >> 6;
    cardinfo->csd.PermWrProtect = (tmp & 0x20) >> 5;
    cardinfo->csd.TempWrProtect = (tmp & 0x10) >> 4;
    cardinfo->csd.FileFormat = (tmp & 0x0C) >> 2;
    cardinfo->csd.ECC = (tmp & 0x03);

    /*!< Byte 15 */
    tmp = (uint8_t)(_csd_tab[3] & 0x000000FF);
    cardinfo->csd.CSD_CRC = (tmp & 0xFE) >> 1;
    cardinfo->csd.Reserved4 = 1;

    /*!< Byte 0 */
    tmp = (uint8_t)((_cid_tab[0] & 0xFF000000) >> 24);
    cardinfo->cid.ManufacturerID = tmp;

    /*!< Byte 1 */
    tmp = (uint8_t)((_cid_tab[0] & 0x00FF0000) >> 16);
    cardinfo->cid.OEM_AppliID = tmp << 8;

    /*!< Byte 2 */
    tmp = (uint8_t)((_cid_tab[0] & 0x000000FF00) >> 8);
    cardinfo->cid.OEM_AppliID |= tmp;

    /*!< Byte 3 */
    tmp = (uint8_t)(_cid_tab[0] & 0x000000FF);
    cardinfo->cid.ProdName1 = tmp << 24;

    /*!< Byte 4 */
    tmp = (uint8_t)((_cid_tab[1] & 0xFF000000) >> 24);
    cardinfo->cid.ProdName1 |= tmp << 16;

    /*!< Byte 5 */
    tmp = (uint8_t)((_cid_tab[1] & 0x00FF0000) >> 16);
    cardinfo->cid.ProdName1 |= tmp << 8;

    /*!< Byte 6 */
    tmp = (uint8_t)((_cid_tab[1] & 0x0000FF00) >> 8);
    cardinfo->cid.ProdName1 |= tmp;

    /*!< Byte 7 */
    tmp = (uint8_t)(_cid_tab[1] & 0x000000FF);
    cardinfo->cid.ProdName2 = tmp;

    /*!< Byte 8 */
    tmp = (uint8_t)((_cid_tab[2] & 0xFF000000) >> 24);
    cardinfo->cid.ProdRev = tmp;

    /*!< Byte 9 */
    tmp = (uint8_t)((_cid_tab[2] & 0x00FF0000) >> 16);
    cardinfo->cid.ProdSN = tmp << 24;

    /*!< Byte 10 */
    tmp = (uint8_t)((_cid_tab[2] & 0x0000FF00) >> 8);
    cardinfo->cid.ProdSN |= tmp << 16;

    /*!< Byte 11 */
    tmp = (uint8_t)(_cid_tab[2] & 0x000000FF);
    cardinfo->cid.ProdSN |= tmp << 8;

    /*!< Byte 12 */
    tmp = (uint8_t)((_cid_tab[3] & 0xFF000000) >> 24);
    cardinfo->cid.ProdSN |= tmp;

    /*!< Byte 13 */
    tmp = (uint8_t)((_cid_tab[3] & 0x00FF0000) >> 16);
    cardinfo->cid.Reserved1 |= (tmp & 0xF0) >> 4;
    cardinfo->cid.ManufactDate = (tmp & 0x0F) << 8;

    /*!< Byte 14 */
    tmp = (uint8_t)((_cid_tab[3] & 0x0000FF00) >> 8);
    cardinfo->cid.ManufactDate |= tmp;

    /*!< Byte 15 */
    tmp = (uint8_t)(_cid_tab[3] & 0x000000FF);
    cardinfo->cid.CID_CRC = (tmp & 0xFE) >> 1;
    cardinfo->cid.Reserved2 = 1;

    return (result);
}

sdcard::OpResult SdcardDriver::_select_deselect(uint64_t addr)
{
    sdcard::OpResult result;

    _command.index = sdcard::SD_CMD_SEL_DESEL_CARD;
    _command.arg = addr;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_SEL_DESEL_CARD);
    return (result);
}

sdcard::OpResult SdcardDriver::_enable_wide_bus_operation(WideBusMode mode)
{
    sdcard::OpResult result = OpResult::OK;

    if (sdcard::SDIO_MULTIMEDIA_CARD == _card_type) {
        result = OpResult::UNSUPPORTED_FEATURE;
        return (result);
    }
    else if ((sdcard::SDIO_STD_CAPACITY_SD_CARD_V1_1 == _card_type) ||
             (sdcard::SDIO_STD_CAPACITY_SD_CARD_V2_0 == _card_type) ||
             (sdcard::SDIO_HIGH_CAPACITY_SD_CARD == _card_type)) {
        switch (mode) {
            case WIDE_BUS_8:
                result = OpResult::UNSUPPORTED_FEATURE;
                return (result);

            case WIDE_BUS_4:
                result = _wide_bus(true);
                if (result == OpResult::OK) {
                    _reconfigure_bus(WideBusMode::WIDE_BUS_4, BusClockDiv::TRANSFER);
                }
                break;

            case WIDE_BUS_1:
            default:
                result = _wide_bus(false);
                if (result == OpResult::OK) {
                    _reconfigure_bus(WideBusMode::WIDE_BUS_1, BusClockDiv::TRANSFER);
                }
                break;
        }
    }

    return (result);
}

sdcard::OpResult SdcardDriver::_stop_transfer(void)
{
    sdcard::OpResult result = OpResult::OK;

    _command.index = sdcard::SD_CMD_STOP_TRANSMISSION;
    _command.arg = 0;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_STOP_TRANSMISSION);

    return (result);
}

sdcard::OpResult SdcardDriver::_get_card_status(sdcard::SdStatusReg* cardstatus)
{
    sdcard::OpResult result = OpResult::OK;
    uint8_t tmp = 0;

    result = _send_sd_status((uint32_t*)_sd_status_tab);

    if (result != OpResult::OK)
        return (result);

    /*!< Byte 0 */
    tmp = (uint8_t)((_sd_status_tab[0] & 0xC0) >> 6);
    cardstatus->DAT_BUS_WIDTH = tmp;

    /*!< Byte 0 */
    tmp = (uint8_t)((_sd_status_tab[0] & 0x20) >> 5);
    cardstatus->SECURED_MODE = tmp;

    /*!< Byte 2 */
    tmp = (uint8_t)((_sd_status_tab[2] & 0xFF));
    cardstatus->SD_CARD_TYPE = tmp << 8;

    /*!< Byte 3 */
    tmp = (uint8_t)((_sd_status_tab[3] & 0xFF));
    cardstatus->SD_CARD_TYPE |= tmp;

    /*!< Byte 4 */
    tmp = (uint8_t)(_sd_status_tab[4] & 0xFF);
    cardstatus->SIZE_OF_PROTECTED_AREA = tmp << 24;

    /*!< Byte 5 */
    tmp = (uint8_t)(_sd_status_tab[5] & 0xFF);
    cardstatus->SIZE_OF_PROTECTED_AREA |= tmp << 16;

    /*!< Byte 6 */
    tmp = (uint8_t)(_sd_status_tab[6] & 0xFF);
    cardstatus->SIZE_OF_PROTECTED_AREA |= tmp << 8;

    /*!< Byte 7 */
    tmp = (uint8_t)(_sd_status_tab[7] & 0xFF);
    cardstatus->SIZE_OF_PROTECTED_AREA |= tmp;

    /*!< Byte 8 */
    tmp = (uint8_t)((_sd_status_tab[8] & 0xFF));
    cardstatus->SPEED_CLASS = tmp;

    /*!< Byte 9 */
    tmp = (uint8_t)((_sd_status_tab[9] & 0xFF));
    cardstatus->PERFORMANCE_MOVE = tmp;

    /*!< Byte 10 */
    tmp = (uint8_t)((_sd_status_tab[10] & 0xF0) >> 4);
    cardstatus->AU_SIZE = tmp;

    /*!< Byte 11 */
    tmp = (uint8_t)(_sd_status_tab[11] & 0xFF);
    cardstatus->ERASE_SIZE = tmp << 8;

    /*!< Byte 12 */
    tmp = (uint8_t)(_sd_status_tab[12] & 0xFF);
    cardstatus->ERASE_SIZE |= tmp;

    /*!< Byte 13 */
    tmp = (uint8_t)((_sd_status_tab[13] & 0xFC) >> 2);
    cardstatus->ERASE_TIMEOUT = tmp;

    /*!< Byte 13 */
    tmp = (uint8_t)((_sd_status_tab[13] & 0x3));
    cardstatus->ERASE_OFFSET = tmp;

    return (result);
}

sdcard::OpResult SdcardDriver::_send_sd_status(uint32_t* psdstatus)
{
    sdcard::OpResult result = OpResult::OK;
    uint32_t count = 0;

    if (sdio_get_card_status_1() & sdcard::SD_CARD_LOCKED) {
        result = OpResult::LOCK_UNLOCK_FAILED;
        return (result);
    }

    _command.index = sdcard::SD_CMD_SET_BLOCKLEN;
    _command.arg = 64;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCKLEN);

    if (result != OpResult::OK)
        return (result);

    _command.index = sdcard::SD_CMD_APP_CMD;
    _command.arg = (uint32_t)(_rca << 16);
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

    if (result != OpResult::OK)
        return (result);

    _data_config.direction = CARD_TO_CONTROLLER;
    _data_config.period = sdcard::SD_DATATIMEOUT;
    _data_config.size = BLOCK_LENGTH_64_B;
    _data_config.length = 64;
    _data_config.mode = BLOCK_DATA_TRANSFER;
    _data_config.enable = true;
    sdio_data_config(&_data_config);

    _command.index = sdcard::SD_CMD_SD_APP_STAUS;
    _command.arg = 0;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_SD_APP_STAUS);

    if (result != OpResult::OK)
        return (result);

    while (!sdio_get_flag_status(SDIO_RXOVERR) && !sdio_get_flag_status(SDIO_DCRCFAIL) &&
           !sdio_get_flag_status(SDIO_DTIMEOUT) && !sdio_get_flag_status(SDIO_DBCKEND) &&
           !sdio_get_flag_status(SDIO_STBITERR)) {
        if (sdio_get_flag_status(SDIO_RXFIFOHF)) {
            for (count = 0; count < 8; count++) {
                *(psdstatus + count) = sdio_read_fifo_data();
            }
            psdstatus += 8;
        }
    }

    if (sdio_get_flag_status(SDIO_DTIMEOUT)) {
        sdio_clear_flag_status(SDIO_DTIMEOUT);
        result = OpResult::DATA_TIMEOUT;
        return (result);
    }

    else if (sdio_get_flag_status(SDIO_DCRCFAIL)) {
        sdio_clear_flag_status(SDIO_DCRCFAIL);
        result = OpResult::DATA_CRC_FAIL;
        return (result);
    }

    else if (sdio_get_flag_status(SDIO_RXOVERR)) {
        sdio_clear_flag_status(SDIO_RXOVERR);
        result = OpResult::RX_OVERRUN;
        return (result);
    }

    else if (sdio_get_flag_status(SDIO_STBITERR)) {
        sdio_clear_flag_status(SDIO_STBITERR);
        result = OpResult::START_BIT_ERR;
        return (result);
    }

    count = sdcard::SD_DATATIMEOUT;
    while (sdio_get_flag_status(SDIO_RXDAVL) && (count > 0)) {
        *psdstatus = sdio_read_fifo_data();
        psdstatus++;
        count--;
    }

    sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);
    return (result);
}

sdcard::OpResult SdcardDriver::_send_status(uint32_t* pcardstatus)
{
    sdcard::OpResult result = OpResult::OK;

    if (pcardstatus == nullptr) {
        result = OpResult::INVALID_PARAMETER;
        return (result);
    }

    _command.index = sdcard::SD_CMD_SEND_STATUS;
    _command.arg = (uint32_t)_rca << 16;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_SEND_STATUS);

    if (result != OpResult::OK) {
        return (result);
    }

    *pcardstatus = sdio_get_card_status_1();

    return (result);
}

CardState SdcardDriver::_get_state(void)
{
    uint32_t resp1 = 0;

    if (detect() == sdcard::SD_PRESENT) {
        if (_send_status(&resp1) != OpResult::OK) {
            return CardState::ERROR;
        }
        else {
            return (CardState)((resp1 >> 9) & 0x0F);
        }
    }
    else {
        return CardState::ERROR;
    }
}

sdcard::OpResult SdcardDriver::_cmd_error(void)
{
    sdcard::OpResult result = OpResult::OK;
    uint32_t timeout;

    timeout = sdcard::SDIO_CMD0TIMEOUT; /*!< 10000 */

    while ((timeout > 0) && !sdio_get_flag_status(SDIO_CMDSENT)) {
        timeout--;
    }

    if (timeout == 0) {
        result = OpResult::CMD_RSP_TIMEOUT;
        return (result);
    }

    sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);
    return (result);
}

sdcard::OpResult SdcardDriver::_cmd_R7_error(void)
{
    sdcard::OpResult result = OpResult::OK;
    uint32_t timeout = sdcard::SDIO_CMD0TIMEOUT;

    while (!sdio_get_flag_status(SDIO_CCRCFAIL) && !sdio_get_flag_status(SDIO_CMDREND) &&
           !sdio_get_flag_status(SDIO_CTIMEOUT) && (timeout > 0))

    {
        timeout--;
    }

    if ((timeout == 0) || sdio_get_flag_status(SDIO_CTIMEOUT)) {
        result = OpResult::CMD_RSP_TIMEOUT;
        sdio_clear_flag_status(SDIO_CTIMEOUT);
        return (result);
    }

    if (sdio_get_flag_status(SDIO_CMDREND)) {
        result = OpResult::OK;
        sdio_clear_flag_status(SDIO_CMDREND);
        return (result);
    }

    return (result);
}

sdcard::OpResult SdcardDriver::_cmd_R1_error(uint8_t cmd)
{
    sdcard::OpResult result = OpResult::OK;
    uint32_t response_r1;

    while (!sdio_get_flag_status(SDIO_CCRCFAIL) && !sdio_get_flag_status(SDIO_CMDREND) &&
           !sdio_get_flag_status(SDIO_CTIMEOUT))
        ;

    if (sdio_get_flag_status(SDIO_CTIMEOUT)) {
        result = OpResult::CMD_RSP_TIMEOUT;
        sdio_clear_flag_status(SDIO_CTIMEOUT);
        return (result);
    }

    else if (sdio_get_flag_status(SDIO_CCRCFAIL)) {
        result = OpResult::CMD_CRC_FAIL;
        sdio_clear_flag_status(SDIO_CCRCFAIL);
        return (result);
    }

    if (sdio_get_cmd_response() != cmd) {
        result = OpResult::ILLEGAL_CMD;
        return (result);
    }

    sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);

    response_r1 = sdio_get_card_status_1();

    if ((response_r1 & sdcard::SD_OCR_ERRORBITS) == sdcard::SD_ALLZERO) {
        return (result);
    }

    if (response_r1 & sdcard::SD_OCR_ADDR_OUT_OF_RANGE) {
        return (OpResult::ADDR_OUT_OF_RANGE);
    }

    if (response_r1 & sdcard::SD_OCR_ADDR_MISALIGNED) {
        return (OpResult::ADDR_MISALIGNED);
    }

    if (response_r1 & sdcard::SD_OCR_BLOCK_LEN_ERR) {
        return (OpResult::BLOCK_LEN_ERR);
    }

    if (response_r1 & sdcard::SD_OCR_ERASE_SEQ_ERR) {
        return (OpResult::ERASE_SEQ_ERR);
    }

    if (response_r1 & sdcard::SD_OCR_BAD_ERASE_PARAM) {
        return (OpResult::BAD_ERASE_PARAM);
    }

    if (response_r1 & sdcard::SD_OCR_WRITE_PROT_VIOLATION) {
        return (OpResult::WRITE_PROT_VIOLATION);
    }

    if (response_r1 & sdcard::SD_OCR_LOCK_UNLOCK_FAILED) {
        return (OpResult::LOCK_UNLOCK_FAILED);
    }

    if (response_r1 & sdcard::SD_OCR_COM_CRC_FAILED) {
        return (OpResult::COM_CRC_FAILED);
    }

    if (response_r1 & sdcard::SD_OCR_ILLEGAL_CMD) {
        return (OpResult::ILLEGAL_CMD);
    }

    if (response_r1 & sdcard::SD_OCR_CARD_ECC_FAILED) {
        return (OpResult::CARD_ECC_FAILED);
    }

    if (response_r1 & sdcard::SD_OCR_CC_ERROR) {
        return (OpResult::CC_ERROR);
    }

    if (response_r1 & sdcard::SD_OCR_GENERAL_UNKNOWN_ERROR) {
        return (OpResult::GENERAL_UNKNOWN_ERROR);
    }

    if (response_r1 & sdcard::SD_OCR_STREAM_READ_UNDERRUN) {
        return (OpResult::STREAM_READ_UNDERRUN);
    }

    if (response_r1 & sdcard::SD_OCR_STREAM_WRITE_OVERRUN) {
        return (OpResult::STREAM_WRITE_OVERRUN);
    }

    if (response_r1 & sdcard::SD_OCR_CID_CSD_OVERWRIETE) {
        return (OpResult::CID_CSD_OVERWRITE);
    }

    if (response_r1 & sdcard::SD_OCR_WP_ERASE_SKIP) {
        return (OpResult::WP_ERASE_SKIP);
    }

    if (response_r1 & sdcard::SD_OCR_CARD_ECC_DISABLED) {
        return (OpResult::CARD_ECC_DISABLED);
    }

    if (response_r1 & sdcard::SD_OCR_ERASE_RESET) {
        return (OpResult::ERASE_RESET);
    }

    if (response_r1 & sdcard::SD_OCR_AKE_SEQ_ERROR) {
        return (OpResult::AKE_SEQ_ERROR);
    }

    return (result);
}

sdcard::OpResult SdcardDriver::_cmd_R3_error(void)
{
    sdcard::OpResult result = OpResult::OK;

    while (!sdio_get_flag_status(SDIO_CCRCFAIL) && !sdio_get_flag_status(SDIO_CMDREND) &&
           !sdio_get_flag_status(SDIO_CTIMEOUT))
        ;

    if (sdio_get_flag_status(SDIO_CTIMEOUT)) {
        result = OpResult::CMD_RSP_TIMEOUT;
        sdio_clear_flag_status(SDIO_CTIMEOUT);
        return (result);
    }

    sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);
    return (result);
}

sdcard::OpResult SdcardDriver::_cmd_R2_error(void)  // sdcard_cmd_R2_error(void)
{
    sdcard::OpResult result = OpResult::OK;

    while (!sdio_get_flag_status(SDIO_CCRCFAIL) && !sdio_get_flag_status(SDIO_CMDREND) &&
           !sdio_get_flag_status(SDIO_CTIMEOUT))
        ;

    if (sdio_get_flag_status(SDIO_CTIMEOUT)) {
        result = OpResult::CMD_RSP_TIMEOUT;
        sdio_clear_flag_status(SDIO_CTIMEOUT);
        return (result);
    }

    else if (sdio_get_flag_status(SDIO_CCRCFAIL)) {
        result = OpResult::CMD_CRC_FAIL;
        sdio_clear_flag_status(SDIO_CCRCFAIL);
        return (result);
    }

    sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);
    return (result);
}

sdcard::OpResult SdcardDriver::_cmd_R6_error(
  uint8_t cmd,
  uint16_t* prca)  // sdcard_cmd_R6_error(uint8_t cmd, uint16_t *prca)
{
    sdcard::OpResult result = OpResult::OK;
    uint32_t response_r1;

    while (!sdio_get_flag_status(SDIO_CCRCFAIL) && !sdio_get_flag_status(SDIO_CMDREND) &&
           !sdio_get_flag_status(SDIO_CTIMEOUT))
        ;

    if (sdio_get_flag_status(SDIO_CTIMEOUT)) {
        result = OpResult::CMD_RSP_TIMEOUT;
        sdio_clear_flag_status(SDIO_CTIMEOUT);
        return (result);
    }

    else if (sdio_get_flag_status(SDIO_CCRCFAIL)) {
        result = OpResult::CMD_CRC_FAIL;
        sdio_clear_flag_status(SDIO_CCRCFAIL);
        return (result);
    }

    if (sdio_get_cmd_response() != cmd) {
        result = OpResult::ILLEGAL_CMD;
        return (result);
    }

    sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);
    response_r1 = sdio_get_card_status_1();

    if (sdcard::SD_ALLZERO ==
        (response_r1 & (sdcard::SD_R6_GENERAL_UNKNOWN_ERROR | sdcard::SD_R6_ILLEGAL_CMD |
                        sdcard::SD_R6_COM_CRC_FAILED))) {
        *prca = (uint16_t)(response_r1 >> 16);
        return (result);
    }

    if (response_r1 & sdcard::SD_R6_GENERAL_UNKNOWN_ERROR) {
        return (OpResult::GENERAL_UNKNOWN_ERROR);
    }

    if (response_r1 & sdcard::SD_R6_ILLEGAL_CMD) {
        return (OpResult::ILLEGAL_CMD);
    }

    if (response_r1 & sdcard::SD_R6_COM_CRC_FAILED) {
        return (OpResult::COM_CRC_FAILED);
    }

    return (result);
}

sdcard::OpResult SdcardDriver::_wide_bus(bool enable)
{
    sdcard::OpResult result = OpResult::OK;
    uint32_t scr[2] = {0, 0};

    if (sdio_get_card_status_1() & sdcard::SD_CARD_LOCKED) {
        result = OpResult::LOCK_UNLOCK_FAILED;
        return (result);
    }

    sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);
    result = _find_SCR(_rca, scr);

    if (result != OpResult::OK)
        return (result);

    if (enable) {
        /*!< If requested card supports wide bus operation */
        if ((scr[1] & sdcard::SD_WIDE_BUS_SUPPORT) != sdcard::SD_ALLZERO) {
            _command.index = sdcard::SD_CMD_APP_CMD;
            _command.arg = (uint32_t)(_rca << 16);
            _command.response = RESPONSE_SHORT;
            _command.wait = WAIT_NO;
            _command.enable_CPSM = true;
            sdio_send_command(&_command);

            result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

            if (result != OpResult::OK)
                return (result);

            _command.index = sdcard::SD_CMD_APP_SD_SET_BUSWIDTH;
            _command.arg = 0x2;
            _command.response = RESPONSE_SHORT;
            _command.wait = WAIT_NO;
            _command.enable_CPSM = true;
            sdio_send_command(&_command);

            result = _cmd_R1_error(sdcard::SD_CMD_APP_SD_SET_BUSWIDTH);

            if (result != OpResult::OK)
                return (result);

            return (result);
        }

        else {
            result = OpResult::REQUEST_NOT_APPLICABLE;
            return (result);
        }
    }

    else {
        /*!< If requested card supports 1 bit mode operation */
        if ((scr[1] & sdcard::SD_SINGLE_BUS_SUPPORT) != sdcard::SD_ALLZERO) {
            _command.index = sdcard::SD_CMD_APP_CMD;
            _command.arg = (uint32_t)(_rca << 16);
            _command.response = RESPONSE_SHORT;
            _command.wait = WAIT_NO;
            _command.enable_CPSM = true;
            sdio_send_command(&_command);

            result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

            if (result != OpResult::OK)
                return (result);

            _command.index = sdcard::SD_CMD_APP_SD_SET_BUSWIDTH;
            _command.arg = 0;
            _command.response = RESPONSE_SHORT;
            _command.wait = WAIT_NO;
            _command.enable_CPSM = true;
            sdio_send_command(&_command);

            result = _cmd_R1_error(sdcard::SD_CMD_APP_SD_SET_BUSWIDTH);

            if (result != OpResult::OK)
                return (result);

            return (result);
        }

        else {
            result = OpResult::REQUEST_NOT_APPLICABLE;
            return (result);
        }
    }
}

void SdcardDriver::_wait_transfer(uint32_t& timeout)
{
    while ((not _is_dma_end_of_transfer) && (not _is_transfer_end) &&
           (_sd_transfer_result == OpResult::OK) && (timeout > 0)) {
        timeout--;
    }
}

OpResult SdcardDriver::_is_busy(CardState& pstatus)
{
    auto result = OpResult::OK;
    uint32_t respR1 = 0;

    _command.index = sdcard::SD_CMD_SEND_STATUS;
    _command.arg = (uint32_t)(_rca << 16);
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    while (!sdio_get_flag_status(SDIO_CCRCFAIL) && !sdio_get_flag_status(SDIO_CMDREND) &&
           !sdio_get_flag_status(SDIO_CTIMEOUT)) {}

    if (sdio_get_flag_status(SDIO_CTIMEOUT)) {
        result = OpResult::CMD_RSP_TIMEOUT;
        sdio_clear_flag_status(SDIO_CTIMEOUT);
        return (result);
    }

    else if (sdio_get_flag_status(SDIO_CCRCFAIL)) {
        result = OpResult::CMD_CRC_FAIL;
        sdio_clear_flag_status(SDIO_CCRCFAIL);
        return (result);
    }

    if ((uint32_t)sdio_get_cmd_response() != sdcard::SD_CMD_SEND_STATUS) {
        result = OpResult::ILLEGAL_CMD;
        return (result);
    }

    sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);

    respR1 = sdio_get_card_status_1();
    pstatus = CardState((uint8_t)((respR1 >> 9) & 0x0000000F));

    if ((respR1 & sdcard::SD_OCR_ERRORBITS) == sdcard::SD_ALLZERO) {
        return (result);
    }

    if (respR1 & sdcard::SD_OCR_ADDR_OUT_OF_RANGE) {
        return (OpResult::ADDR_OUT_OF_RANGE);
    }

    if (respR1 & sdcard::SD_OCR_ADDR_MISALIGNED) {
        return (OpResult::ADDR_MISALIGNED);
    }

    if (respR1 & sdcard::SD_OCR_BLOCK_LEN_ERR) {
        return (OpResult::BLOCK_LEN_ERR);
    }

    if (respR1 & sdcard::SD_OCR_ERASE_SEQ_ERR) {
        return (OpResult::ERASE_SEQ_ERR);
    }

    if (respR1 & sdcard::SD_OCR_BAD_ERASE_PARAM) {
        return (OpResult::BAD_ERASE_PARAM);
    }

    if (respR1 & sdcard::SD_OCR_WRITE_PROT_VIOLATION) {
        return (OpResult::WRITE_PROT_VIOLATION);
    }

    if (respR1 & sdcard::SD_OCR_LOCK_UNLOCK_FAILED) {
        return (OpResult::LOCK_UNLOCK_FAILED);
    }

    if (respR1 & sdcard::SD_OCR_COM_CRC_FAILED) {
        return (OpResult::COM_CRC_FAILED);
    }

    if (respR1 & sdcard::SD_OCR_ILLEGAL_CMD) {
        return (OpResult::ILLEGAL_CMD);
    }

    if (respR1 & sdcard::SD_OCR_CARD_ECC_FAILED) {
        return (OpResult::CARD_ECC_FAILED);
    }

    if (respR1 & sdcard::SD_OCR_CC_ERROR) {
        return (OpResult::CC_ERROR);
    }

    if (respR1 & sdcard::SD_OCR_GENERAL_UNKNOWN_ERROR) {
        return (OpResult::GENERAL_UNKNOWN_ERROR);
    }

    if (respR1 & sdcard::SD_OCR_STREAM_READ_UNDERRUN) {
        return (OpResult::STREAM_READ_UNDERRUN);
    }

    if (respR1 & sdcard::SD_OCR_STREAM_WRITE_OVERRUN) {
        return (OpResult::STREAM_WRITE_OVERRUN);
    }

    if (respR1 & sdcard::SD_OCR_CID_CSD_OVERWRIETE) {
        return (OpResult::CID_CSD_OVERWRITE);
    }

    if (respR1 & sdcard::SD_OCR_WP_ERASE_SKIP) {
        return (OpResult::WP_ERASE_SKIP);
    }

    if (respR1 & sdcard::SD_OCR_CARD_ECC_DISABLED) {
        return (OpResult::CARD_ECC_DISABLED);
    }

    if (respR1 & sdcard::SD_OCR_ERASE_RESET) {
        return (OpResult::ERASE_RESET);
    }

    if (respR1 & sdcard::SD_OCR_AKE_SEQ_ERROR) {
        return (OpResult::AKE_SEQ_ERROR);
    }

    return (result);
}

sdcard::OpResult SdcardDriver::_find_SCR(uint16_t rca, uint32_t* pscr)
{
    uint32_t index = 0;
    sdcard::OpResult result = OpResult::OK;
    uint32_t tempscr[2] = {0, 0};

    _command.index = sdcard::SD_CMD_SET_BLOCKLEN;
    _command.arg = 8;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_SET_BLOCKLEN);

    if (result != OpResult::OK)
        return (result);

    _command.index = sdcard::SD_CMD_APP_CMD;
    _command.arg = (uint32_t)(_rca << 16);
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_APP_CMD);

    if (result != OpResult::OK)
        return (result);

    _data_config.direction = CARD_TO_CONTROLLER;
    _data_config.period = sdcard::SD_DATATIMEOUT;
    _data_config.size = BLOCK_LENGTH_8_B;
    _data_config.length = 8;
    _data_config.mode = BLOCK_DATA_TRANSFER;
    _data_config.enable = true;
    sdio_data_config(&_data_config);

    _command.index = sdcard::SD_CMD_SD_APP_SEND_SCR;
    _command.arg = 0;
    _command.response = RESPONSE_SHORT;
    _command.wait = WAIT_NO;
    _command.enable_CPSM = true;
    sdio_send_command(&_command);

    result = _cmd_R1_error(sdcard::SD_CMD_SD_APP_SEND_SCR);

    if (result != OpResult::OK)
        return (result);

    while (!sdio_get_flag_status(SDIO_RXOVERR) && !sdio_get_flag_status(SDIO_DCRCFAIL) &&
           !sdio_get_flag_status(SDIO_DTIMEOUT) && !sdio_get_flag_status(SDIO_DBCKEND) &&
           !sdio_get_flag_status(SDIO_STBITERR)) {
        if (sdio_get_flag_status(SDIO_RXDAVL)) {
            *(tempscr + index) = sdio_read_fifo_data();
            index++;
        }
    }

    if (sdio_get_flag_status(SDIO_DTIMEOUT)) {
        sdio_clear_flag_status(SDIO_DTIMEOUT);
        result = OpResult::DATA_TIMEOUT;
        return (result);
    }

    else if (sdio_get_flag_status(SDIO_DCRCFAIL)) {
        sdio_clear_flag_status(SDIO_DCRCFAIL);
        result = OpResult::DATA_CRC_FAIL;
        return (result);
    }
    else if (sdio_get_flag_status(SDIO_RXOVERR)) {
        sdio_clear_flag_status(SDIO_RXOVERR);
        result = OpResult::RX_OVERRUN;
        return (result);
    }
    else if (sdio_get_flag_status(SDIO_STBITERR)) {
        sdio_clear_flag_status(SDIO_STBITERR);
        result = OpResult::START_BIT_ERR;
        return (result);
    }

    sdio_clear_flag_status(sdcard::SDIO_STATIC_FLAGS);

    *(pscr + 1) = ((tempscr[0] & sdcard::SD_0TO7BITS) << 24) |
                  ((tempscr[0] & sdcard::SD_8TO15BITS) << 8) |
                  ((tempscr[0] & sdcard::SD_16TO23BITS) >> 8) |
                  ((tempscr[0] & sdcard::SD_24TO31BITS) >> 24);

    *(pscr) = ((tempscr[1] & sdcard::SD_0TO7BITS) << 24) |
              ((tempscr[1] & sdcard::SD_8TO15BITS) << 8) |
              ((tempscr[1] & sdcard::SD_16TO23BITS) >> 8) |
              ((tempscr[1] & sdcard::SD_24TO31BITS) >> 24);

    return (result);
}

void SdcardDriver::_init_sdio_periph()
{
    using Gpio = cm3cpp::gpio::Gpio;

    auto pin_setup = [](Gpio p, Gpio::PullMode mode) {
        p.set_af(config::sdio::ALT_FUNC);
        p.mode_setup(Gpio::Mode::ALTERNATE_FUNCTION, mode);
        p.set_output_options(Gpio::OutputType::PUSH_PULL, Gpio::Speed::FAST_50MHz);
    };

    for (const auto& pin : config::sdio::DATA_PINS) {
        pin_setup(pin, Gpio::PullMode::PULL_UP);
    }
    pin_setup(config::sdio::CK_PIN, Gpio::PullMode::PULL_UP);
    pin_setup(config::sdio::CMD_PIN, Gpio::PullMode::NO_PULL);

    _sd_det.init(config::sdio::SD_DET_PIN);
    _sd_det.mode_setup(Gpio::Mode::INPUT, Gpio::PullMode::PULL_UP);

    nvic_set_priority(config::sdio::IRQ, config::sdio::IRQ_PRIORITY);
    nvic_set_priority(config::sdio::dma::IRQ, config::sdio::dma::IRQ_PRIORITY);

    nvic_enable_irq(config::sdio::IRQ);
    nvic_enable_irq(config::sdio::dma::IRQ);
}

void SdcardDriver::_setup_dma(uint32_t* buffer, uint32_t size, ExchangeDir dir)
{
    auto const DMA = config::sdio::dma::PERIPH;
    auto const DMA_STREAM = config::sdio::dma::STREAM;

    sdio_enable_interrupt(SDIO_DCRCFAIL | SDIO_DTIMEOUT | SDIO_DATAEND | SDIO_RXOVERR |
                          SDIO_STBITERR);
    sdio_enable_dma();

    dma_clear_interrupt_flags(DMA, DMA_STREAM,
                              (DMA_HISR_FEIF4 | DMA_HISR_FEIF4 | DMA_HISR_FEIF4 |
                               DMA_HISR_FEIF4 | DMA_HISR_FEIF4));

    dma_disable_stream(DMA, DMA_STREAM);
    dma_stream_reset(DMA, DMA_STREAM);
    dma_channel_select(DMA, DMA_STREAM, config::sdio::dma::CHANNEL);
    dma_set_peripheral_address(DMA, DMA_STREAM, (uint32_t)&SDIO_FIFO);
    dma_set_memory_address(DMA, DMA_STREAM, (uint32_t)buffer);

    if (dir == ExchangeDir::RECEIVE) {
        dma_set_transfer_mode(DMA, DMA_STREAM, DMA_SxCR_DIR_PERIPHERAL_TO_MEM);
    }
    else {
        dma_set_transfer_mode(DMA, DMA_STREAM, DMA_SxCR_DIR_MEM_TO_PERIPHERAL);
    }

    dma_set_number_of_data(DMA, DMA_STREAM, size);
    dma_disable_peripheral_increment_mode(DMA, DMA_STREAM);
    dma_enable_memory_increment_mode(DMA, DMA_STREAM);
    dma_set_peripheral_size(DMA, DMA_STREAM, DMA_SxCR_PSIZE_32BIT);
    dma_set_memory_size(DMA, DMA_STREAM, DMA_SxCR_MSIZE_32BIT);
    dma_set_priority(DMA, DMA_STREAM, DMA_SxCR_PL_VERY_HIGH);
    dma_enable_fifo_mode(DMA, DMA_STREAM);
    dma_set_fifo_threshold(DMA, DMA_STREAM, DMA_SxFCR_FS_LT_4_4_FULL);
    dma_set_memory_burst(DMA, DMA_STREAM, DMA_SxCR_MBURST_INCR4);
    dma_set_peripheral_burst(DMA, DMA_STREAM, DMA_SxCR_PBURST_INCR4);
    dma_enable_transfer_complete_interrupt(DMA, DMA_STREAM);
    dma_set_peripheral_flow_control(DMA, DMA_STREAM);
    dma_enable_stream(DMA, DMA_STREAM);
}

void SdcardDriver::_reconfigure_bus(WideBusMode wide, BusClockDiv clock_div)
{
    _transfer_config.bus_wide = wide;
    _transfer_config.clock_divider = to_number(clock_div);

    sdio_config(&_transfer_config);
}

} /* namespace sdcard */

} /* namespace hw */

extern "C" {
void sdio_isr()
{
    hw::sdcard::SdcardDriver::Instance()._sdio_irq_proc();
}

void dma2_stream3_isr(void)
{
    hw::sdcard::SdcardDriver::Instance()._dma_irq_proc();
}
}
