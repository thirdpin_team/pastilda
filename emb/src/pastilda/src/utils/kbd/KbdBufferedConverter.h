/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_KBD_KBD_BUFFERED_CONVERTER_HPP_
#define UTILS_KBD_KBD_BUFFERED_CONVERTER_HPP_

#include <cstddef>
#include <cstring>

#include <etl/set.h>
#include <etl/vector.h>
#include <strs/StringView.hpp>

#include <hw/kbd/Package.hpp>

#include "KeyObj.h"

namespace utils {

namespace kbd {

using std::memcmp;
using std::size_t;

// DESCRIPTION
// Keyboard have strange logic. If you press keys 1, 2 and 3 simultaneously
// then you will see input package like {0, 0, 0, 1, 2, 3, 0, 0, 0}. But if you
// send 1, 2 and 3 one by one very fast, then you'll see packages like:
//   1. {0, 0, 0, 1, 0, 0, 0, 0, 0}
//   2. {0, 0, 0, 1, 2, 0, 0, 0, 0}
//   3. {0, 0, 0, 2, 3, 0, 0, 0, 0}
//   4. {0, 0, 0, 3, 0, 0, 0, 0, 0}
// So this class converts this strange logic to linear.

class KbdBufferedConverter final
{
 public:
    explicit KbdBufferedConverter();
    ~KbdBufferedConverter() = default;

    void process(const hw::kbd::Package& package_ptr);
    strs::StringView get_line();

 private:
    using AsciiBuffer = etl::vector<char, 128>;
    AsciiBuffer _ascii_buffer;
    AsciiBuffer::iterator _buffer_pos;

    static constexpr uint32_t MAX_SAME_KEYS_COUNT_BEFORE_OUTPUT = 5;

    const hw::kbd::Package* _input_package;
    hw::kbd::Package _last_package;

    class KeySet final : public etl::set<hw::kbd::Key, hw::kbd::PACKAGE_LENGTH>
    {
     public:
        using set::set;

        bool is_include(key_type key)
        {
            auto res = this->find(key);
            if (res == this->end()) {
                return false;
            }
            return true;
        }
    };

    KeySet _last_keys_set;

    uint32_t _same_keys = 0;
    bool _is_many_same_keys = false;

    void _store_key(const kbd::KeyObj& key);
    void _store_input_package();
    void _store_input_package_series();

    bool _is_series_package_too_big();

    void _reset_input();
};

} /* namespace kbd */

} /* namespace utils */

#endif /* UTILS_KBD_KBD_BUFFERED_CONVERTER_HPP_ */
