/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKS_COMMON_FIFO_HPP_
#define TASKS_COMMON_FIFO_HPP_

#include <queue.hpp>

namespace rtos = cpp_freertos;

namespace common {

template<class T, uint32_t N>
class Fifo : public rtos::Queue
{
 public:
    using FifoType = T;
    static constexpr uint32_t ITEMS_COUNT = N;

    Fifo() : Queue(N, sizeof(T)) {}

    virtual ~Fifo() = default;
};

} /* namespace common */

#endif /* TASKS_COMMON_FIFO_HPP_ */
