/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TREE_H_
#define TREE_H_

#include <etl/iterator.h>
#include <functional>
#include <string>

#include <utils/memory/TypedPool.hpp>

#include "TreeNode.hpp"

namespace tree {

template<std::size_t size, typename T>
class Tree
{
 public:
    static constexpr std::size_t NODES_COUNT = size;

    using Node = TreeNode<T>;

    using NodeContainer = typename Node::Container;

    using Pool = utils::memory::TypedPool<Node, NODES_COUNT>;

    using iterator = typename Pool::iterator;

    Tree();
    Tree(Node* node, Pool* pool);
    ~Tree();

    size_t get_current_child_num();

    void add_child(const NodeContainer& container);
    void add_child();
    void add_neighbor(const NodeContainer& container);
    void add_neighbor();

    void move_left();
    void move_right();
    void move_into();
    void move_out();

    void move_in_head();
    void move_most_right();
    void move_most_left();

    const Node& walk_from_first_to(const Node& node);

    iterator begin() { return _pool->begin(); }

    iterator end() { return _pool->end(); }

    bool is_most_left()
    {
        return (_current_node->get_left_neighbor() == nullptr);
    }

    bool is_most_right()
    {
        return (_current_node->get_right_neighbor() == nullptr);
    }

    bool is_most_top() { return (_current_node->get_parent() == nullptr); }

    bool is_most_bottom() { return (_current_node->isEndNode()); }

    Node* get_current_node() { return _current_node; }

    bool is_full() { return _pool->full(); }

    Node* _allocate_tree_node() { return _pool->allocate_and_construct(); }

    void destroy();

 protected:
    Pool* _pool;
    Node* _current_node;
    Node* const _head_node;

    void _destroy_node(Node* node);
    void _destroy_tree(Node* tree);
};

template<std::size_t size, typename T>
Tree<size, T>::Tree() :
  _pool(new Pool()),
  _current_node(_allocate_tree_node()),
  _head_node(_current_node)
{
}

template<std::size_t size, typename T>
Tree<size, T>::Tree(Node* node, Pool* pool) :
  _pool(pool),
  _current_node(node),
  _head_node(_current_node)
{
}

template<std::size_t size, typename T>
Tree<size, T>::~Tree()
{
    delete _pool;
}

template<std::size_t size, typename T>
size_t Tree<size, T>::get_current_child_num()
{
    Node* node = _current_node;
    size_t child_num = node->get_parent()->get_children_count();

    while (node != nullptr) {
        child_num--;
        node = node->getLeftNeighbor();
    }

    child_num =
      _current_node->get_parent()->get_children_count() - child_num - 1;

    return child_num;
}

template<std::size_t size, typename T>
void Tree<size, T>::add_child(const NodeContainer& container)
{
    if (this->is_full() == false) {
        Node* child = _allocate_tree_node();
        child->set_container(container);

        _current_node->add_child(child);
    }
}

template<std::size_t size, typename T>
void Tree<size, T>::add_child()
{
    if (this->is_full() == false) {
        Node* child = _allocate_tree_node();

        _current_node->add_child(child);
    }
}

template<std::size_t size, typename T>
void Tree<size, T>::add_neighbor(const NodeContainer& container)
{
    if (this->is_full() == false) {
        Node* neighbor = _pool->allocate();
        neighbor->set_container(container);

        _current_node->add_right_neighbor(neighbor);
    }
}

template<std::size_t size, typename T>
void Tree<size, T>::add_neighbor()
{
    if (this->is_full() == false) {
        Node* neighbor = _allocate_tree_node();

        _current_node->add_right_neighbor(neighbor);
    }
}

template<std::size_t size, typename T>
void Tree<size, T>::move_left()
{
    if (this->is_most_left() == false) {
        _current_node = _current_node->get_left_neighbor();
    }
}

template<std::size_t size, typename T>
void Tree<size, T>::move_right()
{
    if (this->is_most_right() == false) {
        _current_node = _current_node->get_right_neighbor();
    }
}

template<std::size_t size, typename T>
void Tree<size, T>::move_into()
{
    if (_current_node->is_end_node() == false) {
        _current_node = _current_node->get_child_at(0);
    }
}

template<std::size_t size, typename T>
void Tree<size, T>::move_out()
{
    if (this->is_most_top() == false) {
        _current_node = _current_node->get_parent();
    }
}

template<std::size_t size, typename T>
void Tree<size, T>::destroy()
{
    _pool->release_all();
    _current_node = _allocate_tree_node();
}

template<std::size_t size, typename T>
void Tree<size, T>::move_in_head()
{
    _current_node = _head_node;
}

template<std::size_t size, typename T>
void Tree<size, T>::move_most_right()
{
    while (is_most_right() == false) {
        move_right();
    }
}

template<std::size_t size, typename T>
void Tree<size, T>::move_most_left()
{
    while (is_most_left() == false) {
        move_left();
    }
}

template<std::size_t size, typename T>
auto Tree<size, T>::walk_from_first_to(const Node& node) -> const Node&
{
    static auto iter = _pool->begin();

    if (&(*iter) == &node) {
        iter = _pool->begin();

        return node;
    }

    decltype(iter) result;

    if (iter == _pool->begin()) {
        result = _pool->begin();
    }
    else {
        result = iter;
    }

    iter++;

    return *result;
}

template<std::size_t size, typename T>
void Tree<size, T>::_destroy_tree(Node* tree)
{
    const std::size_t CHILDREN_COUNT = tree->get_children_count();
    for (std::size_t i = 0; i < CHILDREN_COUNT; ++i) {
        Node* child = tree->get_child_at(CHILDREN_COUNT - i - 1);
        if (child->is_end_node()) {
            _destroy_node(child);
        }
        else {
            _destroy_tree(child);
        }
    }
    _destroy_node(tree);
}

template<std::size_t size, typename T>
void Tree<size, T>::_destroy_node(Node* node)
{
    destroy_tree_node(node);
}

} /* namespace tree */

#endif /* TREE_H_ */
