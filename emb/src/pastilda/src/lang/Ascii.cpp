/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Ascii.h"

namespace lang {

constexpr static std::size_t SHIFT_FOR_SHIFTED_ASCII_CODES = 0x21;
static Ascii::Code
  ascii_shifted[Ascii::CODES_COUNT - SHIFT_FOR_SHIFTED_ASCII_CODES] = {
    Ascii::Code::NUL,                // EXCLAMATION_MARK
    Ascii::Code::NUL,                // DOUBLE_QUOTES
    Ascii::Code::NUL,                // NUMBER
    Ascii::Code::NUL,                // DOLLAR
    Ascii::Code::NUL,                // PERCENT
    Ascii::Code::NUL,                // AMPERSAND
    Ascii::Code::DOUBLE_QUOTES,      // SINGLE_QUOTE
    Ascii::Code::NUL,                // OPEN_PARENTHESIS
    Ascii::Code::NUL,                // CLOSE_PARENTHESIS
    Ascii::Code::NUL,                // ASTERISK
    Ascii::Code::NUL,                // PLUS
    Ascii::Code::LESS_THAN,          // COMMA
    Ascii::Code::UNDERSCORE,         // HYPHEN
    Ascii::Code::GREATER_THAN,       // PERIOD_OR_DOT
    Ascii::Code::QUESTION_MARK,      // SLASH_OR_DIVIDE
    Ascii::Code::CLOSE_PARENTHESIS,  // ZERO
    Ascii::Code::EXCLAMATION_MARK,   // ONE
    Ascii::Code::AT_SYMBOL,          // TWO
    Ascii::Code::NUMBER,             // THREE
    Ascii::Code::DOLLAR,             // FOUR
    Ascii::Code::PERCENT,            // FIVE
    Ascii::Code::CARET,              // SIX
    Ascii::Code::AMPERSAND,          // SEVEN
    Ascii::Code::ASTERISK,           // EIGHT
    Ascii::Code::OPEN_PARENTHESIS,   // NINE
    Ascii::Code::NUL,                // COLON
    Ascii::Code::COLON,              // SEMICOLON
    Ascii::Code::NUL,                // LESS_THAN
    Ascii::Code::PLUS,               // EQUALS
    Ascii::Code::NUL,                // GREATER_THAN
    Ascii::Code::NUL,                // QUESTION_MARK
    Ascii::Code::NUL,                // AT_SYMBOL
    Ascii::Code::NUL,                // UPPERCASE_A
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::NUL,            // UPPERCASE_Z
    Ascii::Code::OPENING_BRACE,  // OPENING_BRACKET
    Ascii::Code::VERTICAL_BAR,   // BACKSLASH
    Ascii::Code::CLOSING_BRACE,  // CLOSING_BRACKET
    Ascii::Code::NUL,
    Ascii::Code::NUL,
    Ascii::Code::TILDE_OR_EQUIVALENCY_SIGN,  // GRAVE_ACCENT
    Ascii::Code::UPPERCASE_A,                // LOWERCASE_A
    Ascii::Code::UPPERCASE_B,
    Ascii::Code::UPPERCASE_C,
    Ascii::Code::UPPERCASE_D,
    Ascii::Code::UPPERCASE_E,
    Ascii::Code::UPPERCASE_F,
    Ascii::Code::UPPERCASE_G,
    Ascii::Code::UPPERCASE_H,
    Ascii::Code::UPPERCASE_I,
    Ascii::Code::UPPERCASE_J,
    Ascii::Code::UPPERCASE_K,
    Ascii::Code::UPPERCASE_L,
    Ascii::Code::UPPERCASE_M,
    Ascii::Code::UPPERCASE_N,
    Ascii::Code::UPPERCASE_O,
    Ascii::Code::UPPERCASE_P,
    Ascii::Code::UPPERCASE_Q,
    Ascii::Code::UPPERCASE_R,
    Ascii::Code::UPPERCASE_S,
    Ascii::Code::UPPERCASE_T,
    Ascii::Code::UPPERCASE_U,
    Ascii::Code::UPPERCASE_V,
    Ascii::Code::UPPERCASE_W,
    Ascii::Code::UPPERCASE_X,
    Ascii::Code::UPPERCASE_Y,
    Ascii::Code::UPPERCASE_Z,  // LOWERCASE_Z
    Ascii::Code::NUL,          // OPENING_BRACE
    Ascii::Code::NUL,          // VERTICAL_BAR
    Ascii::Code::NUL,          // CLOSING_BRACE
    Ascii::Code::NUL,          // TILDE_OR_EQUIVALENCY_SIGN
    Ascii::Code::NUL           // DELETE
};

constexpr static std::size_t SHIFT_FOR_SHIFTED_BACK_ASCII_CODES = 0x21;
static Ascii::Code ascii_shifted_back[Ascii::CODES_COUNT -
                                      SHIFT_FOR_SHIFTED_BACK_ASCII_CODES] = {
  Ascii::Code::ONE,              // EXCLAMATION_MARK
  Ascii::Code::SINGLE_QUOTE,     // DOUBLE_QUOTES
  Ascii::Code::THREE,            // NUMBER
  Ascii::Code::FOUR,             // DOLLAR
  Ascii::Code::FIVE,             // PERCENT
  Ascii::Code::SEVEN,            // AMPERSAND
  Ascii::Code::NUL,              // SINGLE_QUOTE
  Ascii::Code::NINE,             // OPEN_PARENTHESIS
  Ascii::Code::ZERO,             // CLOSE_PARENTHESIS
  Ascii::Code::EIGHT,            // ASTERISK
  Ascii::Code::EQUALS,           // PLUS
  Ascii::Code::NUL,              // COMMA
  Ascii::Code::NUL,              // HYPHEN
  Ascii::Code::NUL,              // PERIOD_OR_DOT
  Ascii::Code::NUL,              // SLASH_OR_DIVIDE
  Ascii::Code::NUL,              // ZERO
  Ascii::Code::NUL,              // ONE
  Ascii::Code::NUL,              // TWO
  Ascii::Code::NUL,              // THREE
  Ascii::Code::NUL,              // FOUR
  Ascii::Code::NUL,              // FIVE
  Ascii::Code::NUL,              // SIX
  Ascii::Code::NUL,              // SEVEN
  Ascii::Code::NUL,              // EIGHT
  Ascii::Code::NUL,              // NINE
  Ascii::Code::SEMICOLON,        // COLON
  Ascii::Code::NUL,              // SEMICOLON
  Ascii::Code::COMMA,            // LESS_THAN
  Ascii::Code::NUL,              // EQUALS
  Ascii::Code::PERIOD_OR_DOT,    // GREATER_THAN
  Ascii::Code::SLASH_OR_DIVIDE,  // QUESTION_MARK
  Ascii::Code::TWO,              // AT_SYMBOL
  Ascii::Code::LOWERCASE_A,      // UPPERCASE_A
  Ascii::Code::LOWERCASE_B,
  Ascii::Code::LOWERCASE_C,
  Ascii::Code::LOWERCASE_D,
  Ascii::Code::LOWERCASE_E,
  Ascii::Code::LOWERCASE_F,
  Ascii::Code::LOWERCASE_G,
  Ascii::Code::LOWERCASE_H,
  Ascii::Code::LOWERCASE_I,
  Ascii::Code::LOWERCASE_J,
  Ascii::Code::LOWERCASE_K,
  Ascii::Code::LOWERCASE_L,
  Ascii::Code::LOWERCASE_M,
  Ascii::Code::LOWERCASE_N,
  Ascii::Code::LOWERCASE_O,
  Ascii::Code::LOWERCASE_P,
  Ascii::Code::LOWERCASE_Q,
  Ascii::Code::LOWERCASE_R,
  Ascii::Code::LOWERCASE_S,
  Ascii::Code::LOWERCASE_T,
  Ascii::Code::LOWERCASE_U,
  Ascii::Code::LOWERCASE_V,
  Ascii::Code::LOWERCASE_W,
  Ascii::Code::LOWERCASE_X,
  Ascii::Code::LOWERCASE_Y,
  Ascii::Code::LOWERCASE_Z,  // UPPERCASE_Z
  Ascii::Code::NUL,          // OPENING_BRACKET
  Ascii::Code::NUL,          // BACKSLASH
  Ascii::Code::NUL,          // CLOSING_BRACKET
  Ascii::Code::SIX,          // CARET
  Ascii::Code::HYPHEN,       // UNDERSCORE
  Ascii::Code::NUL,          // GRAVE_ACCENT
  Ascii::Code::NUL,          // LOWERCASE_A
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,
  Ascii::Code::NUL,              // LOWERCASE_Z
  Ascii::Code::OPENING_BRACKET,  // OPENING_BRACE
  Ascii::Code::SLASH_OR_DIVIDE,  // VERTICAL_BAR
  Ascii::Code::CLOSING_BRACKET,  // CLOSING_BRACE
  Ascii::Code::GRAVE_ACCENT,     // TILDE_OR_EQUIVALENCY_SIGN
  Ascii::Code::NUL               // DELETE
};

Ascii::Code get_ascii_shifted(Ascii::Code code)
{
    auto aCode = static_cast<Ascii::Type>(code);
    if (aCode >= SHIFT_FOR_SHIFTED_ASCII_CODES) {
        return ascii_shifted[aCode - SHIFT_FOR_SHIFTED_ASCII_CODES];
    }
    else {
        return Ascii::Code::NUL;
    }
}

Ascii::Code get_ascii_shifted_back(Ascii::Code code)
{
    int aCode = static_cast<Ascii::Type>(code);
    if (aCode >= (int)SHIFT_FOR_SHIFTED_BACK_ASCII_CODES and aCode < Ascii::CODES_COUNT) {
        return ascii_shifted_back[aCode - SHIFT_FOR_SHIFTED_BACK_ASCII_CODES];
    }
    else {
        return Ascii::Code::NUL;
    }
}

bool is_ascii_shifted(Ascii::Code code)
{
    return (get_ascii_shifted_back(code) != Ascii::Code::NUL);
}

Ascii Ascii::get_shifted() const
{
    return get_ascii_shifted(_ascii);
}

Ascii Ascii::get_shifted_back() const
{
    return get_ascii_shifted_back(_ascii);
}

bool Ascii::is_shifted() const
{
    return is_ascii_shifted(_ascii);
}

} /* namespace lang */
