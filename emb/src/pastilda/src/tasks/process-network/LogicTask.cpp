/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <strs/StringFixed.hpp>

#include "LogicTask.h"
#include <lang/Ascii.h>

namespace tasks {

void LogicTask::Run()
{
    while (true) {
        _blocking_read();
        _state_machine_proc();
    }
}

void LogicTask::_state_machine_proc()
{
    StateAction act;
    while (true) {
        switch (_state) {
            case State::IDLE:
                act = _idle_proc();
                if (act == StateAction::NEXT) {
                    _state = State::INVITATION;
                }
                if (act == StateAction::STAY) {
                    return;
                }
                break;

            case State::INVITATION:
                act = _invitation_proc();
                if (act == StateAction::NEXT) {
                    _state = State::AUTH;
                    return;
                }
                break;

            case State::AUTH:
                act = _auth_proc();
                if (act == StateAction::NEXT) {
                    _state = State::DECRYPT;
                }
                if (act == StateAction::QUIT) {
                    _state = State::ENDING;
                }
                if (act == StateAction::STAY) {
                    return;
                }
                break;

            case State::DECRYPT:
                act = _decrypt_proc();
                if (act == StateAction::NEXT) {
                    _state = State::DB_WAIT;
                    return;
                }
                break;

            case State::DB_WAIT:
                act = _db_wait_proc();
                if (act == StateAction::NEXT) {
                    _state = State::MENU_START;
                }
                if (act == StateAction::FAIL) {
                    _state = State::DB_ERROR;
                }
                if (act == StateAction::QUIT) {
                    _state = State::ENDING;
                }
                if (act == StateAction::STAY) {
                    return;
                }
                break;

            case State::DB_ERROR:
                act = _db_error_proc();
                if (act == StateAction::NEXT) {
                    _state = State::INVITATION;
                }
                if (act == StateAction::QUIT) {
                    _state = State::ENDING;
                }
                if (act == StateAction::STAY) {
                    return;
                }
                break;

            case State::MENU_START:
                act = _menu_start_proc();
                if (act == StateAction::NEXT) {
                    _state = State::NAVIGATION;
                    return;
                }
                break;

            case State::NAVIGATION:
                act = _navigation_proc();
                if (act == StateAction::NEXT) {
                    _state = State::LOGIN;
                }
                if (act == StateAction::QUIT) {
                    _state = State::ENDING;
                }
                if (act == StateAction::STAY) {
                    return;
                }
                break;

            case State::LOGIN:
                act = _login_proc();
                if (act == StateAction::NEXT) {
                    _state = State::LOGIN_WAIT;
                    return;
                }
                if (act == StateAction::FAIL) {
                    _state = State::NAVIGATION;
                    return;
                }
                break;

            case State::LOGIN_WAIT:
                act = _login_wait_proc();
                if (act == StateAction::NEXT) {
                    _state = State::ENDING;
                }
                break;

            case State::ENDING:
                act = _ending_proc();
                if (act == StateAction::NEXT) {
                    _state = State::END;
                    return;
                }
                break;

            case State::END:
                act = _end_proc();
                if (act == StateAction::NEXT) {
                    _state = State::IDLE;
                    return;
                }
                if (act == StateAction::STAY) {
                    return;
                }
                break;
        }
    }
}

auto LogicTask::_idle_proc() -> StateAction
{
    if (_fifo_data.token == msg::LogicEvent::PASTILDA_MODE) {
        return StateAction::NEXT;
    }

    return StateAction::STAY;
}

auto LogicTask::_invitation_proc() -> StateAction
{
    OutputManagerTask::MsgType message;
    message.token = OutputManagerTask::MsgType::Token::PASTILDA_START;
    message.data.str = ">>>";
    OutputManagerTask::fifo_push(&message);

    _password.clear();

    return StateAction::NEXT;
}

auto LogicTask::_auth_proc() -> StateAction
{
    if (_fifo_data.token != msg::LogicEvent::ASCII_KEY) {
        return StateAction::STAY;
    }

    strs::StringFixed<1> output_symb = "*";

    _key.set(_fifo_data.data);
    if (_key.is_control() == true) {
        if (_key.get_ascii() == lang::Ascii::ESC) {
            return StateAction::QUIT;
        }

        if (_key.get_ascii() == lang::Ascii::LF) {
            return StateAction::NEXT;
        }

        return StateAction::STAY;
    }

    if (_key.get_ascii() == lang::Ascii::BACKSPACE) {
        _password.pop_back();
        output_symb = static_cast<char>(_key.get_ascii());
    }
    else {
        _password.add(static_cast<char>(_key.get_ascii()));
        // TODO: JUST FOR TEST!
        // output_symb = static_cast<char>(_key.get_ascii());
    }

    OutputManagerTask::MsgType message;
    message.token = OutputManagerTask::MsgType::Token::PASSWORD;
    message.data.str = output_symb;
    OutputManagerTask::fifo_push(&message);

    return StateAction::STAY;
}

auto LogicTask::_decrypt_proc() -> StateAction
{
    DatabaseTask::MsgType db_message;
    db_message.token = DatabaseTask::MsgType::Token::DECRYPT;
    db_message.data = strs::StringView(_password.data(), _password.length());
    DatabaseTask::fifo_push(&db_message);

    return StateAction::NEXT;
}

auto LogicTask::_db_wait_proc() -> StateAction
{
    if ((_fifo_data.token == msg::LogicEvent::ASCII_KEY) &&
        (_fifo_data.data == lang::Ascii::ESC)) {
        return StateAction::QUIT;
    }

    if (_fifo_data.token == msg::LogicEvent::DB_ERROR) {
        return StateAction::FAIL;
    }

    if (_fifo_data.token == msg::LogicEvent::DB_READY) {
        return StateAction::NEXT;
    }

    return StateAction::STAY;
}

auto LogicTask::_db_error_proc() -> StateAction
{
    if (_fifo_data.token != msg::LogicEvent::ASCII_KEY) {
        return StateAction::STAY;
    }

    _key.set(_fifo_data.data);

    if (_key.get_ascii() == lang::Ascii::ESC) {
        return StateAction::QUIT;
    }

    if (_key.get_ascii() == lang::Ascii::LF) {
        return StateAction::NEXT;
    }

    return StateAction::STAY;
}

auto LogicTask::_menu_start_proc() -> StateAction
{
    _navigator.move_top();
    _display_node_name();

    return StateAction::NEXT;
}

auto LogicTask::_navigation_proc() -> StateAction
{
    if (_fifo_data.token != msg::LogicEvent::ASCII_KEY) {
        return StateAction::STAY;
    }

    _key.set(_fifo_data.data);
    lang::Ascii ascii = _fifo_data.data;

    if (not _key.is_control()) {
        return StateAction::STAY;
    }

    if (_key.get_ascii() == lang::Ascii::ESC) {
        return StateAction::QUIT;
    }

    _navigator.process_key(_key.get_ascii());

    if (_navigator.is_end_node()) {
        return StateAction::NEXT;
    }

    _display_node_name();

    return StateAction::STAY;
}

auto LogicTask::_login_proc() -> StateAction
{
    auto contaiter = _navigator.get_current_point_container();
    if (contaiter.get_password().length() == 0 &&
        contaiter.get_login().length() == 0 &&
        contaiter.get_sequence().length() == 0) {
        return StateAction::FAIL;
    }

    DatabaseTask::MsgType db_message;
    db_message.token = DatabaseTask::MsgType::Token::DECRYPT_PASS;
    DatabaseTask::fifo_push(&db_message);

    return StateAction::NEXT;
}

auto LogicTask::_login_wait_proc() -> StateAction
{
    if (_fifo_data.token == msg::LogicEvent::LOGIN_ENDED) {
        return StateAction::NEXT;
    }

    return StateAction::STAY;
}

auto LogicTask::_ending_proc() -> StateAction
{
    OutputManagerTask::MsgType disp_message;
    disp_message.token = OutputManagerTask::MsgType::Token::END;
    OutputManagerTask::fifo_push(&disp_message);

    return StateAction::NEXT;
}

auto LogicTask::_end_proc() -> StateAction
{
    if (_fifo_data.token == msg::LogicEvent::DISP_ENDED) {
        DatabaseTask::MsgType db_message;
        db_message.token = DatabaseTask::MsgType::Token::DB_CLOSE;
        DatabaseTask::fifo_push(&db_message);

        SwitcherTask::MsgType switch_message;
        switch_message.token = SwitcherTask::MsgType::Token::PASTILD_END;
        SwitcherTask::fifo_push(&switch_message);

        return StateAction::NEXT;
    }

    return StateAction::STAY;
}

void LogicTask::_display_node_name()
{
    auto contaiter = _navigator.get_current_point_container();

    OutputManagerTask::MsgType message;
    message.token = OutputManagerTask::MsgType::Token::MESSAGE;
    message.data.str = contaiter.get_name();
    OutputManagerTask::fifo_push(&message);
}

} /* namespace tasks */
