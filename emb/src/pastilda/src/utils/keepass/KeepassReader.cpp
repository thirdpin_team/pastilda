/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "KeepassReader.h"

namespace utils {

namespace keepass {

constexpr uint8_t KeepassReader::IV_SALSA[8];
KeepassReader::ReadState KeepassReader::_reader_state;

void KeepassReader::set_credentials(const char* pass, uint32_t len)
{
    _credentials = Credentials::PASSWORD;

    if (len > 0) {
        memcpy(_pass, pass, len);
        _pass_len = len;
    }
    else {
        _pass_len = 0;
    }
}

auto KeepassReader::check_db_file(const char* db_name) -> DecryptionResult
{
    if (_fs.open_file_to_read(&_file, db_name) != FR_OK) {
        return (DecryptionResult::DB_FILE_ERROR);
    }

    _reader_state.decrypt_in_progress = true;
    return DecryptionResult::SUCCESS;
}

auto KeepassReader::check_db_version() -> DecryptionResult
{
    uint32_t signature1 = 0;
    uint32_t signature2 = 0;

    _fs.read_next_file_chunk(&_file, &signature1, sizeof(uint32_t));
    _fs.read_next_file_chunk(&_file, &signature2, sizeof(uint32_t));

    bool sig1correct = (signature1 == SIGNATURE_1);
    bool sig2correct = ((signature2 == SIGNATURE_2_2X_PRE_RELEASE) ||
                        (signature2 == SIGNATURE_2_2X_POST_RELEASE));

    if (sig1correct && sig2correct) {
        return (DecryptionResult::SUCCESS);
    }

    _fs.close_file(&_file);
    _reader_state.decrypt_in_progress = false;
    return (DecryptionResult::SIGNATURE_ERROR);
}

auto KeepassReader::check_db_credentials() -> DecryptionResult
{
    if (_credentials == Credentials::NOT_SELECTED) {
        _fs.close_file(&_file);
        _reader_state.decrypt_in_progress = false;
        return (DecryptionResult::CREDENTIALS_ERROR);
    }

    _readHeader();

    if (_credentials == Credentials::PASSWORD)
        _makeMasterKey(_pass, _pass_len);

    if (_credentials == Credentials::KEY_FILE)
        _makeMasterKey(_keyf, _keyf_len);

    if (_credentials == Credentials::PASSWORD_AND_KEY_FILE)
        _makeMasterKey(_pass, _keyf, _pass_len, _keyf_len);

    _fs.read_next_file_chunk(&_file, _decrypted_data, _header[STREAM_START_BYTES].size);

    KeepassCrypto::AES_CBC_init(_master_key, _header[ENCRYPTION_IV].data);
    KeepassCrypto::AES_CBC_proc(_decrypted_data, _header[STREAM_START_BYTES].size);

    if (memcmp(_header[STREAM_START_BYTES].data, _decrypted_data,
               _header[STREAM_START_BYTES].size)) {
        _fs.close_file(&_file);
        _reader_state.decrypt_in_progress = false;
        return (DecryptionResult::MASTER_KEY_ERROR);
    }

    return (DecryptionResult::SUCCESS);
}

void KeepassReader::read_start()
{
    _fs.read_next_file_chunk(&_file, _decrypted_data, DECRYPTED_BLOCK_SIZE);
    KeepassCrypto::AES_CBC_proc(_decrypted_data, DECRYPTED_BLOCK_SIZE);
}

auto KeepassReader::read_next_chunk(uint8_t* buffer, uint32_t* len) -> DecryptionResult
{

    uint8_t hash[HASH_LENGTH] = {0};

    if (_reader_state.decrypt_in_progress == false) {
        return DecryptionResult::DB_FILE_CLOSED;
    }

    if (_reader_state.new_block) {
        _reader_state.new_block = false;
        memset(&_reader_state.block_data, 0, sizeof(BlockDataHeader));

        KeepassCrypto::SHA256_init(&_reader_state.sha);

        memcpy(&_reader_state.block_data, &_decrypted_data[_reader_state.offset],
               sizeof(BlockDataHeader));

        if (_reader_state.block_data.blockDataSize == 0) {
            read_stop();
            return DecryptionResult::SUCCESS;
        }

        _reader_state.block_size =
          DECRYPTED_BLOCK_SIZE - _reader_state.offset - sizeof(BlockDataHeader);
        _reader_state.sha.data_in = _reader_state.block_size;

        memcpy(_reader_state.sha.buffer,
               &_decrypted_data[_reader_state.offset + sizeof(BlockDataHeader)],
               _reader_state.block_size);
        KeepassCrypto::SHA256_proc(&_reader_state.sha);

        memcpy(buffer, &_decrypted_data[_reader_state.offset + sizeof(BlockDataHeader)],
               _reader_state.block_size);
        len[0] = _reader_state.block_size;

        return DecryptionResult::NONE;
    }

    else {
        _fs.read_next_file_chunk(&_file, _decrypted_data, DECRYPTED_BLOCK_SIZE);
        KeepassCrypto::AES_CBC_proc(_decrypted_data, DECRYPTED_BLOCK_SIZE);

        if ((_reader_state.block_data.blockDataSize - _reader_state.block_size) >=
            DECRYPTED_BLOCK_SIZE) {
            _reader_state.block_size += DECRYPTED_BLOCK_SIZE;
            memcpy(&_reader_state.sha.buffer[_reader_state.sha.data_in], _decrypted_data,
                   DECRYPTED_BLOCK_SIZE);
            _reader_state.sha.data_in += DECRYPTED_BLOCK_SIZE;
            KeepassCrypto::SHA256_proc(&_reader_state.sha);

            memcpy(buffer, _decrypted_data, DECRYPTED_BLOCK_SIZE);
            len[0] = DECRYPTED_BLOCK_SIZE;

            return DecryptionResult::NONE;
        }

        else {
            uint32_t data_left =
              _reader_state.block_data.blockDataSize - _reader_state.block_size;
            _reader_state.block_size += data_left;
            memcpy(&_reader_state.sha.buffer[_reader_state.sha.data_in], _decrypted_data,
                   data_left);
            _reader_state.sha.data_in += data_left;
            KeepassCrypto::SHA256_proc(&_reader_state.sha);

            memcpy(buffer, _decrypted_data, data_left);
            len[0] = data_left;

            KeepassCrypto::SHA256_stop(&_reader_state.sha, hash);
            _reader_state.new_block = true;

            if (memcmp(_reader_state.block_data.blockDataHash, hash, HASH_LENGTH) != 0) {
                read_stop();
                return DecryptionResult::DATA_HASH_ERROR;
            }

            _reader_state.offset = data_left;
            return DecryptionResult::NONE;
        }
    }

    return DecryptionResult::NONE;
}

void KeepassReader::read_stop()
{
    _reader_state.offset = 0;
    _reader_state.new_block = true;
    KeepassCrypto::AES_CBC_stop();
    _fs.close_file(&_file);
    _reader_state.decrypt_in_progress = false;
}

void KeepassReader::reset_password_decryption()
{
    _pass_reset = true;
}

void KeepassReader::decrypt_password(Password* password)
{
    uint32_t decoded_len = 0;
    static uint8_t hash[HASH_LENGTH];
    static uint8_t decrypted_pass[MAX_PASSWORD_SIZE_IN_BYTES] = {0};

    if (_pass_reset) {
        KeepassCrypto::SHA256_eval(_header[PROTECTED_STREAM_KEY].data,
                                   _header[PROTECTED_STREAM_KEY].size, hash);

        KeepassCrypto::init_Salsa20(hash, IV_SALSA);

        _pass_reset = false;
    }

    Base64::Decode(password->data(), password->length(), (char*)decrypted_pass,
                   MAX_PASSWORD_SIZE_IN_BYTES, &decoded_len);

    KeepassCrypto::eval_Salsa20(decrypted_pass, decoded_len);

    password->set((const char*)decrypted_pass, decoded_len);
}

void KeepassReader::_readHeader()
{
    uint8_t nfield;
    uint32_t signature3 = 0;
    memset(_header, 0, sizeof(_header));

    // first 4 bytes in header = database version
    _fs.read_next_file_chunk(&_file, &signature3, sizeof(uint32_t));

    // data in header organized as follows:
    // 1 byte = number of header field
    // 2 bytes = size of header filed
    //[size of header filed] bytes
    for (int i = 0; i < HEADER_FIELD_COUNT; i++) {
        _fs.read_next_file_chunk(&_file, &nfield, sizeof(uint8_t));

        if (nfield < HEADER_FIELD_COUNT) {
            _fs.read_next_file_chunk(&_file, &_header[nfield].size, sizeof(uint16_t));
            _fs.read_next_file_chunk(&_file, _header[nfield].data, _header[nfield].size);
        }
        if (nfield == END_OF_HEADER) {
            break;
        }
    }
}

void KeepassReader::_makeMasterKey(uint8_t* key, uint32_t key_len)
{
    uint8_t hash1[HASH_LENGTH];
    uint8_t hash2[HASH_LENGTH];

    KeepassCrypto::SHA256_eval(key, key_len, hash1);
    KeepassCrypto::SHA256_eval(hash1, HASH_LENGTH, hash2);

    _makeKeyRoutine(hash2);
}

void KeepassReader::_makeMasterKey(uint8_t* pass,
                                   uint8_t* keyfile,
                                   uint32_t pass_len,
                                   uint32_t keylile_len)
{
    uint8_t composite[COMPOSITE_KEY_LENGTH];
    uint8_t composite_hash[HASH_LENGTH];

    // getting hash of a password and key file
    KeepassCrypto::SHA256_eval(pass, pass_len, &composite[0]);
    KeepassCrypto::SHA256_eval(keyfile, keylile_len, &composite[HASH_LENGTH]);

    // getting hash of a composite key
    KeepassCrypto::SHA256_eval(composite, COMPOSITE_KEY_LENGTH, composite_hash);

    // the main make key routine is the same as for keepass v.1
    _makeKeyRoutine(composite_hash);
}

void KeepassReader::_makeKeyRoutine(uint8_t* key_hash)
{
    uint8_t final_key[MASTER_KEY_LENGTH_2X];
    memcpy(&final_key[0], _header[MASTER_SEED].data, _header[MASTER_SEED].size);

    uint32_t rounds = (*(uint32_t*)_header[TRANSFORM_ROUNDS].data);

    KeepassCrypto::AES_EBC_encrypt(_header[TRANSFORM_SEED].data, key_hash, HASH_LENGTH,
                                   rounds);

    KeepassCrypto::SHA256_eval(key_hash, HASH_LENGTH,
                               &final_key[_header[MASTER_SEED].size]);
    KeepassCrypto::SHA256_eval(final_key, MASTER_KEY_LENGTH_2X, _master_key);
}

} /* namespace keepass */

} /* namespace utils */
