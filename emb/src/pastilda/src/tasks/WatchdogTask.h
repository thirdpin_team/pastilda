/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin <d.lisin@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKS_WATCHDOGTASK_H_
#define TASKS_WATCHDOGTASK_H_

#include <config/TasksConfig.h>
#include <thread.hpp>
#include <ticks.hpp>

namespace tasks {

namespace rtos = cpp_freertos;

class WatchdogTask final : rtos::Thread
{
 public:
    WatchdogTask();
    ~WatchdogTask() = default;

 private:
    void Run();
};

} /* namespace tasks */

#endif /* TASKS_WATCHDOGTASK_H_ */
