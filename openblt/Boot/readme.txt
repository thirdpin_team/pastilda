LED indicate OpenBLT state:

= NORMAL STATE =
green blinking		-> working
cyan blinking		-> checking new firmware file
blue just light		-> erasing flash-memory
magenta blinking	-> programming flash-memory


= ERROR STATE =
yellow blinking		-> something wrong, but flash-memory content is not touched
red blinking		-> critical error: flash-memory content is affected, firmware can be damaged

= OTHER =
white blinking		-> Pastilda firmware is missing or corrupted
