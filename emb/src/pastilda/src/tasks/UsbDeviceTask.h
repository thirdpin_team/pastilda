/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *  Dmitrii Lisin       <d.lisin@thirdpin.ru>
 *  Ilya Stolyarov      <i.stolyarov@thirdpin.ru>
 *  Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKS_USBDEVICE_H_
#define TASKS_USBDEVICE_H_

#include <libopencm3/stm32/f4/nvic.h>

#include <critical.hpp>
#include <queue.hpp>
#include <semaphore.hpp>
#include <thread.hpp>
#include <ticks.hpp>

#include <config/TasksConfig.h>
#include <hw/UsbDevice.h>
#include <hw/kbd/Package.hpp>
#include <utils/patterns/Singleton.hpp>

#include "process-network/SwitcherTask.h"

namespace tasks {

namespace rtos = cpp_freertos;

class UsbDeviceTask final
  : rtos::Thread
  , public utils::patterns::Singleton<UsbDeviceTask>
{
    friend struct utils::patterns::Singleton<UsbDeviceTask>;

    friend void ::otg_fs_isr();

 public:
    static void enqueue(hw::kbd::Package package);

 private:
    enum class Action
    {
        SEND,
        NONE
    };

    friend struct StaticAccess;
    struct StaticAccess
    {
        static void set_semaphore_interrupt(usbd_device* dev, unsigned char);

        static usbd_request_return_codes usb_control_callback(
          usbd_device*,
          struct usb_setup_data*,
          uint8_t** buf,
          uint16_t* len,
          usbd_control_complete_callback*);

        static void usb_set_config_callback(usbd_device*, uint16_t wValue);

        StaticAccess() = delete;
    };

    hw::usb::UsbDevice _usb;

    rtos::BinarySemaphore _semaphore_interrupt;
    uint8_t _keyboard_protocol = 1;
    uint8_t _keyboard_idle = 0;

    rtos::Queue _input_queue;
    hw::kbd::Package _package;

    UsbDeviceTask();

    void Run();

    usbd_request_return_codes _hid_control_request(usbd_device*,
                                                   struct usb_setup_data*,
                                                   uint8_t** buf,
                                                   uint16_t* len,
                                                   void (**)(usbd_device* usbd_dev,
                                                             struct usb_setup_data*));

    void _hid_set_config(usbd_device* usbd_dev, uint16_t wValue);

    uint16_t _usb_send_packet_nonblock(hw::kbd::Package* package);

    usbd_request_return_codes _usb_control_callback(usbd_device*,
                                                    struct usb_setup_data*,
                                                    uint8_t** buf,
                                                    uint16_t* len,
                                                    usbd_control_complete_callback*);

    void _usb_set_config_callback(usbd_device* usbd_dev, uint16_t wValue);

    void _set_semaphore_interrupt(usbd_device* dev, unsigned char);

    Action _dequeue();
};

} /* namespace tasks */

#endif /* TASKS_USBDEVICE_H_ */
