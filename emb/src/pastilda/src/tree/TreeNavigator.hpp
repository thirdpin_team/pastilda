/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TREE_TREENAVIGATOR_H_
#define TREE_TREENAVIGATOR_H_

#include "Tree.hpp"
#include <lang/Ascii.h>

namespace tree {

template<typename T>
class TreeNavigator
{
 public:
    using Tree = T;
    using NodeContainer = typename Tree::NodeContainer;

    void init(Tree* tree) { _tree = tree; }

    void process_key(lang::Ascii ascii);

    bool is_end_node()
    {
        bool is_end_node = _is_end_node;
        _is_end_node = false;

        return is_end_node;
    }

    void move_top();

    NodeContainer& get_current_point_container();

    TreeNavigator();
    TreeNavigator(Tree* tree);
    ~TreeNavigator();

 private:
    Tree* _tree;
    bool _is_end_node = false;

    void _process_enter_key();
};

template<typename T>
TreeNavigator<T>::TreeNavigator(Tree* tree) : _tree(tree)
{
}

template<typename T>
TreeNavigator<T>::~TreeNavigator()
{
}

template<typename T>
inline void TreeNavigator<T>::move_top()
{
    _tree->move_in_head();    // maximum top
    _tree->move_most_left();  // maximum left
}

template<typename T>
void TreeNavigator<T>::process_key(lang::Ascii ascii)
{
    switch (ascii.code()) {
        case lang::Ascii::UP:
            _tree->move_left();
            break;

        case lang::Ascii::DOWN:
            _tree->move_right();
            break;

        case lang::Ascii::RIGHT:
            _tree->move_into();
            break;

        case lang::Ascii::LEFT:
            _tree->move_out();
            break;

        case lang::Ascii::LF:
            _process_enter_key();
            break;

        default:
            break;
    }
}

template<typename T>
typename TreeNavigator<T>::NodeContainer&
TreeNavigator<T>::get_current_point_container()
{
    return _tree->get_current_node()->get_container();
}

template<typename T>
void TreeNavigator<T>::_process_enter_key()
{
    auto current_point = _tree->get_current_node();

    if (current_point->is_end_node()) {
        _is_end_node = true;
    }
    else {
        _tree->move_into();
    }
}

}  // namespace tree

#endif /* TREE_TREENAVIGATOR_H_ */
