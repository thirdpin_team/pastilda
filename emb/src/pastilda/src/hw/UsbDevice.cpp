/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <libopencm3/usb/dwc/otg_fs.h>

#include "UsbDevice.h"

namespace hw {

namespace usb {

constexpr uint8_t UsbDevice::keyboard_report_descriptor[];
constexpr UsbDevice::type_hid_function UsbDevice::keyboard_hid_function;
constexpr struct usb_device_descriptor UsbDevice::dev;
constexpr struct usb_endpoint_descriptor UsbDevice::hid_endpoint;
constexpr struct usb_endpoint_descriptor UsbDevice::msc_endpoint[];
constexpr struct usb_interface_descriptor UsbDevice::iface[];
constexpr struct usb_interface UsbDevice::ifaces[];
constexpr struct usb_config_descriptor UsbDevice::config_descr;

UsbDevice::UsbDevice() :
  _device(usbd_init(&otgfs_usb_driver,
                    &dev,
                    &config_descr,
                    usb_strings.data(),
                    usb_strings.size(),
                    _buffer.data(),
                    _buffer.size()))
{
    using Gpio = cm3cpp::gpio::Gpio;

    Gpio uf_p(config::USB_DEVICE_P);
    Gpio uf_m(config::USB_DEVICE_M);

    uf_p.mode_setup(Gpio::Mode::ALTERNATE_FUNCTION, Gpio::PullMode::NO_PULL);
    uf_p.set_af(config::USB_DEVICE_AF);

    uf_m.mode_setup(Gpio::Mode::ALTERNATE_FUNCTION, Gpio::PullMode::NO_PULL);
    uf_m.set_af(config::USB_DEVICE_AF);
}

void UsbDevice::msd_init()
{
    auto& fs = hw::FileSystem::Instance();

    usb_msc_init(_device, Endpoint::E_MASS_STORAGE_IN, 64, Endpoint::E_MASS_STORAGE_OUT,
                 64, "ThirdPin", "Pastilda", "0.01", "Pastilda2",
                 hw::FileSystem::StaticAccess::msd_blocks(),
                 hw::FileSystem::StaticAccess::msd_read,
                 hw::FileSystem::StaticAccess::msd_write);
}

void UsbDevice::enable_interrupt()
{
    nvic_set_priority(hw::config::USB_DEVICE_IRQ, hw::config::USB_DEVICE_IRQ_PRIORITY);
    nvic_enable_irq(hw::config::USB_DEVICE_IRQ);
}

void UsbDevice::disable_interrupt()
{
    nvic_disable_irq(hw::config::USB_DEVICE_IRQ);
}

// TODO: [USB] move is_host_suspend && remote_wakeup to driver, remove magic
// numbers
bool UsbDevice::is_host_suspend(void)
{
    return ((OTG_FS_DSTS & 0x01) & 1);  // bit SUSPSTS: Suspend status
}

void UsbDevice::remote_wakeup(bool on)
{
    if (on)
        OTG_FS_DCTL |= 0x00000001;  // bit RWUSIG: Remote wakeup signaling
    else
        OTG_FS_DCTL &= ~0x00000001;
}

} /* namespace usb */

} /* namespace hw */
