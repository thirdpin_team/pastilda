@ECHO OFF
for /f %%a in ('dir /B /S *.hex') do %~dp0DfuFileMgr.exe %%a %%a.dfu >nul
%~dp0DfuSeCommand.exe -c -d --fn boot.hex.dfu -l
TIMEOUT 1 >nul
%~dp0DfuSeCommand.exe -l >nul