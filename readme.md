# **PASTILDA: open-source password manager**
-----------

1. /dfu contains everything you need for loading and updating the firmware and bootloader. See instructions below or read /dfu/readme.txt.
2. /emb contains Pastilda project. We are using Eclipse + GNU ARM Eclipse + OpenOCD. You can read about it here: http://gnuarmeclipse.github.io.
3. /openbit contains open-source bootloader project. Use EmBitz IDE to build or change it.
4. /release contains all releases we provide.

## **Description**
-----------

It is an open-source hardware designed to manage our credentials in handy and secure way.  It provides easy and safe auto-login to bank accounts, mailboxes, corporate network or social media. So if you have to daily remember and enter a number of strong passwords — assign it to Pastilda.


## **Advantages**
----------
  * Pastilda never reveals master key to the host. 
  * Decrypted data stays onboard, unreachable for malware. 
  * Requires no drivers or client software. 
  * Pastilda impersonates a common keyboard, so it works with most systems by default.
  * Summon a control menu to any text field.
  * Applicable for command line interfaces. 


## **Complete Openness**
-----------------

Publishes under GNU GPL license. Feel free to use both software and hardware in your own projects.


## **How Does It Work**
----------------

Pastilda has two USB slots: one to connect keyboard, one to connect it to your PC. So, your OS recognize Pastilda as composite device: 
keyboard and flash drive. And your real keyboard now is visible just for Pastilda, your PC doesn't see it at all.

On flash drive you should store KeePass 2.x portable and encrypted database (.kdbx file).

In a regular mode, everyting you type on you keyboard translates through Pastilda to your PC.

If you need to sign in somewhere, you have to switch to "Pastilda mode". To do that you have to set your mouse cursor inside a login textbox and 
then type combination Ctrl + ~. After that, Pastilda will ask you to enter password from your KeePass database. 

If password is correct Pastilda decrypts your database and you can start to navigate trough it with arrows left, right, up and down.
As soon as you find the resource you were looking for, you should type Enter and after that, Pastilda automaticly substitutes the corresponding
username and password in appropriate textboxes and enters you account.

If password is incorrect Pastilda will offer you to enter password again. So, you can either continue to enter password or go back to 
regular mode by pressing Esc key on your keyboard.


## **How to Load New Firmware Using DFU**
----------------------------------

You can find an instructions in dfu folder.


## **How to Update Firmware**
----------------------

If you want to load updated firmware to your device:

  * We will load each new firmware to the release folder. 
  * For updating your device you will need just copy the latest pastilda.srec file to the Pastilda.
  * Then unplug and plug your device. After that the bootloader will start to work.
  * Programming is finished when LEDs on your Pastilda start to blink with 1s period. 
  * Notice that pastilda.srec file on your Pastilda has dissapeared.
  * That's it! Now you can connect your keyboard to Pastilda and try it's new functionality.


## **What LEDs blinking on My Pastilda Means?!**
-----------------------------------------

Now watch the LED on the board. It must go through the next states (you can also watch a video here: https://youtu.be/eg9DWGGIzso):
1. Green flash at a start
2. Blue light blinking
3. Blue light (no blinking)
4. Blue light blinking
5. The colors starts to change each other with 1 second period, a new disk appears at your PC file system. It means, that the programming is finished. This is good Pastilda's behavior.


## **Play With Our Pre-Loaded Database!**
----------------------------------

If you are using Pastilda for the first time start with our sample database db.kdbx located in dfu/sdcard folder:

  1. Copy this file to your Pastilda.
  2. Open Notepad and press Ctrl + ~. Now your keyboard will work in 'Pastilda Mode'.
  3. You will see Pastilda's invitation: >>>
  4. Now you have to enter the password (QWErty for db.kdbx) and press Enter.
  5. After that Pastilda will decrypt the database and you can start to navigate trough it with arrows left, right, up and down. 
  6. To leave 'Pastilda Mode' just press Esc.
  7. In our pre-loaded database there are just two entries: openbit and wikipedia.
  8. Once you choose resource you want to log in (openbit or wikipedia) just press Enter.
  9. OK! This was is a little demonstration. Now you can try it to really log in somewhere.
  10. Go to ideone.com or wikipedia.org and repeat steps 1 to 8.
  11. That's it! Now you can replace demo db.kdbx file with your own database.


## **Known Limitations For .kdbx file**
--------------------------------

* Your database should be renamed to db.kdbx.
* db.kdbx file must be located in a root directory.
+ At this moment we do not support GZip, so you have to turn it off:
      * *Open your database with KeePass.exe.*
      * *Go to File -> Database Settings -> Compression and select 'None'.*
      * *Press OK and save all changes.*
+ At this moment you can store just up to 350 enrties in your database including stuff in Recycle Bin. You can just disable it:
      * *Open your database with KeePass.exe.*
      * *Go to File -> Database Settings -> Recycle Bin and uncheck 'Use a recycle bin' check box.*
      * *Press OK and save all changes.*
+ We do not support History of an entry. If you have made any changes to your database you have to clean up History:
      * *Open your database with KeePass.exe.*
      * *Go to Tools -> Database Tools -> Database Maintenance.*
      * *Set 'Delete history entries older than (days)' to 0.*
      * *Delete 'Entry history' and 'Deleted objects information'.*
      * *Press Close and save all changes.*
* We support just latin symbols for Title, User name and Password fields. So, if you use your native language for any of these fields you have to change it. Otherwise, Pastilda will return an error: '!LATIN SYMBOLS ONLY ALLOWED!'.


## **Contacts**
--------

If you have any questions, please feel free to contact us:

  * Anastasiia Lazareva, developer: <a.lazareva@thirdpin.ru>
  * Dmitrii Lisin, developer: <d.lisin@thirdpin.ru>
  * Ilya Stolyarov: <i.stolyarov@thirdpin.ru>
  * Pavel Larionov: <p.larionov@thirdpin.ru>
