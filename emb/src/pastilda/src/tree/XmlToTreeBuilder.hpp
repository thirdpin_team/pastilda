/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TREEBUILDER_HPP_
#define TREEBUILDER_HPP_

#include <cstring>

#include <etl/set.h>
#include <lang/Ascii.h>
#include <strs/StringFixed.hpp>
#include <strs/StringView.hpp>
#include <utils/memory/Dict.hpp>
#include <yxml/yxml.h>

#include "Tree.hpp"

namespace tree {

namespace detail {

enum class Tag
{
    NO_TAG,
    ENTRY,
    GROUP,
    VALUE,
    KEY,
    NAME,
    HISTORY,
    KEEPASS,
    DEFAULT_SEQUENCE,
    BINARY,
    META,
};

enum TagType
{
    END_TAG,
    START_TAG
};

enum class KeyType
{
    NO_KEY,
    USERNAME,
    PASSWORD,
    TYPE,
    TITLE
};

enum class Attr
{
    PROTECTED,
    NO_ATTR
};

static constexpr size_t PROCESSING_CONTENT_BUFFER_SIZE = 128;

struct TagContent
{
    bool in_processing;
    bool trailed_symb_skipped;
    strs::StringFixed<PROCESSING_CONTENT_BUFFER_SIZE> content;
};

struct AttrContent
{
    bool in_processing;
    strs::StringFixed<PROCESSING_CONTENT_BUFFER_SIZE> content;
};

struct _TAG_DICT : public utils::memory::HashDict<strs::StringView, Tag, 10>
{
    _TAG_DICT()
    {
        _add(Key("Group"), Tag::GROUP);
        _add(Key("Entry"), Tag::ENTRY);
        _add(Key("Name"), Tag::NAME);
        _add(Key("Key"), Tag::KEY);
        _add(Key("Value"), Tag::VALUE);
        _add(Key("History"), Tag::HISTORY);
        _add(Key("KeePassFile"), Tag::KEEPASS);
        _add(Key("DefaultSequence"), Tag::DEFAULT_SEQUENCE);
        _add(Key("Binary"), Tag::BINARY);
        _add(Key("Meta"), Tag::META);

        set_default(Tag::NO_TAG);
    }
} static TAG_DICT;

struct _KEY_DICT : public utils::memory::FlatDict<strs::StringView, KeyType, 3>
{
    _KEY_DICT()
    {
        _add(Key("Title"), KeyType::TITLE);
        _add(Key("UserName"), KeyType::USERNAME);
        _add(Key("Password"), KeyType::PASSWORD);

        set_default(KeyType::NO_KEY);
    }
} static KEY_DICT;

struct _ATTR_DICT : public utils::memory::FlatDict<strs::StringView, Attr, 2>
{
    _ATTR_DICT()
    {
        _add(Key("Protected"), Attr::PROTECTED);
        _add(Key("ProtectInMemory"), Attr::PROTECTED);

        set_default(Attr::NO_ATTR);
    }
} static ATTR_DICT;

struct _IGNORE_SET
{
 public:
    _IGNORE_SET()
    {
        _set.insert(Tag::HISTORY);
        _set.insert(Tag::BINARY);
        _set.insert(Tag::META);
    }

    bool is_include(Tag tag)
    {
        auto res = _set.find(tag);
        if (res == _set.end()) {
            return false;
        }
        return true;
    }

 private:
    etl::set<Tag, 3> _set;
} static IGNORES;

namespace AttrValStrings {
static const strs::StringView TRUE = "True";
static const strs::StringView FALSE = "False";
}  // namespace AttrValStrings

namespace MarkStrings {
static const strs::StringView GROUP = "f";
static const strs::StringView ENTRY = " ";
}  // namespace MarkStrings

}  // namespace detail

namespace _ = detail;

using std::memcmp;
using std::strcmp;

template<typename T1, typename T2>
class XmlToTreeBuilder
{
 public:
    using RawTree = yxml_t;

    struct XmlSource
    {
        uint8_t* data;
        uint32_t length;
    };

    enum class TreeState
    {
        FINISHED,
        BROKEN,
        AT_WORK
    };

    using Tree = T1;
    using TreeNode = typename Tree::Node;
    using NodeContainer = typename Tree::NodeContainer;

    using StringPool = T2;
    using StringPoolValue = typename StringPool::value_type;

    static constexpr size_t PROCESSING_STACK_SIZE = 2048;
    static constexpr size_t MAXIMAL_DEEPNESS = 32;

    XmlToTreeBuilder(Tree* tree, StringPool* string_pool) :
      _string_pool(string_pool),
      _xml_source{nullptr, 0},
      _buffer_ptr(nullptr),
      _tree(tree)
    {
        static_assert(sizeof(StringPoolValue) == sizeof(char),
                      "Char-like type should be here!");
        _reset();
    }

    ~XmlToTreeBuilder() {}

    TreeState build_first_block(XmlSource& xml_source);
    TreeState build_next_block(XmlSource& xml_file);

 private:
    StringPool* _string_pool;
    using DeepnessString = strs::StringFixed<MAXIMAL_DEEPNESS>;
    DeepnessString _deepness_string;

    XmlSource _xml_source;
    uint8_t* _buffer_ptr;

    Tree* _tree;
    RawTree _src_tree;
    uint8_t _processing_stack[PROCESSING_STACK_SIZE];

    int32_t _tree_deepness;
    bool _is_first_tree_elem;
    bool _is_tree_ended;
    bool _is_ignore_mode;
    bool _is_tree_building_started;

    TreeState _tree_state;

    _::Tag _current_tag;
    _::Tag _last_parent_tag;
    _::KeyType _current_key;
    _::KeyType _last_key;
    _::Attr _current_attr;

    _::TagContent _current_tag_content;
    _::AttrContent _current_attr_content;

    void _process();
    void _reset();

    void _process_tag_start();
    void _process_attr_content();
    void _process_attr_end();
    void _process_tag_content();
    void _process_tag_end();
    bool _is_symb_forbidden(const char c);

    void _add_tree_node();
    void _fill_entry_fields();
    void _fill_group_fields();
    void _fill_sequence_field();
    DeepnessString& _get_deepness_string();

    char* _allocate_in_pool(const char* data, uint32_t length);
    char* _allocate_in_pool(strs::StringView s);

    void _update_current_tag(_::TagType type);
    void _update_current_key();
    void _update_current_attr();
    void _check_end_of_tree();
    void _finish_tree();
    strs::StringView _get_tag_name(_::TagType type);

    bool _is_tag();
    bool _is_attr();
    bool _is_contain_text();
    bool _is_node_of_tree();
    bool _is_top_group_skipped();
};

template<typename T1, typename T2>
auto XmlToTreeBuilder<T1, T2>::build_first_block(XmlSource& xml_source)
  -> XmlToTreeBuilder<T1, T2>::TreeState
{
    _tree_state = TreeState::AT_WORK;

    _tree->destroy();
    _reset();

    _is_tree_building_started = true;
    _xml_source = xml_source;

    _process();

    return _tree_state;
}

template<typename T1, typename T2>
auto XmlToTreeBuilder<T1, T2>::build_next_block(XmlSource& xml_file)
  -> XmlToTreeBuilder<T1, T2>::TreeState
{
    if (not _is_tree_building_started) {
        return TreeState::BROKEN;
    }

    _tree_state = TreeState::AT_WORK;

    _xml_source = xml_file;
    _process();

    return _tree_state;
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_reset()
{
    _tree_deepness = -1;
    _is_first_tree_elem = true;
    _is_tree_ended = false;
    _is_ignore_mode = false;
    _is_tree_building_started = false;
    _current_tag = _::Tag::NO_TAG;
    _last_parent_tag = _::Tag::NO_TAG;
    _current_key = _::KeyType::NO_KEY;
    _last_key = _::KeyType::NO_KEY;
    _current_attr = _::Attr::NO_ATTR;

    _current_tag_content.content.clear();
    _current_tag_content.in_processing = false;
    _current_tag_content.trailed_symb_skipped = false;

    _current_attr_content.content.clear();
    _current_attr_content.in_processing = false;

    _xml_source = {nullptr, 0};

    yxml_init(&_src_tree, _processing_stack, PROCESSING_STACK_SIZE);
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_process()
{
    _buffer_ptr = _xml_source.data;
    auto _buffer_len = _xml_source.length;

    for (uint32_t i = 0; i < _buffer_len; _buffer_ptr++, i++) {
        yxml_ret_t elem = yxml_parse(&_src_tree, *_buffer_ptr);

        if (_is_tree_ended) {
            _finish_tree();
            return;
        }

        if (_tree_state == TreeState::BROKEN) {
            _tree->destroy();
            _reset();
            return;
        }

        switch (elem) {
            case YXML_ELEMSTART:
                _update_current_tag(_::START_TAG);

                if (_::IGNORES.is_include(_current_tag)) {
                    _is_ignore_mode = true;
                }

                if (_is_tag() && not _is_ignore_mode) {
                    _process_tag_start();
                }
                break;

            case YXML_ATTRSTART:
                if (not _is_ignore_mode) {
                    _update_current_attr();
                }
                break;

            case YXML_ATTRVAL:
                if (_is_attr() && not _is_ignore_mode) {
                    _process_attr_content();
                }
                break;

            case YXML_ATTREND:
                if (_is_attr() && not _is_ignore_mode) {
                    _process_attr_end();
                }
                break;

            case YXML_CONTENT:
                if (not _is_ignore_mode) {
                    _process_tag_content();
                }
                break;

            case YXML_ELEMEND:
                _update_current_tag(_::END_TAG);

                if (_is_tag() && _is_top_group_skipped() &&
                    not _is_ignore_mode) {
                    _process_tag_end();
                }

                if (_::IGNORES.is_include(_current_tag)) {
                    _is_ignore_mode = false;
                }

                _check_end_of_tree();
                break;

            default:
                break;
        }
    }

    if (_is_tree_ended) {
        _finish_tree();
    }
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_process_tag_start()
{
    _current_attr_content.content.clear();

    _current_tag_content.trailed_symb_skipped = false;
    _current_tag_content.in_processing = false;

    if (_is_node_of_tree()) {
        _tree_deepness++;
        if (_is_top_group_skipped()) {
            _last_parent_tag = _current_tag;
            _add_tree_node();
        }
    }
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_add_tree_node()
{
    if (_tree_deepness > 1) {
        _tree->add_child();
        _tree->move_into();
        _tree->move_most_right();
    }
    else {
        if (not _is_first_tree_elem) {
            _tree->add_neighbor();
            _tree->move_right();
        }
        else {
            _is_first_tree_elem = false;
        }
    }
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_process_tag_content()
{
    if (_is_contain_text() && _is_top_group_skipped()) {
        _current_tag_content.in_processing = true;
        char c = *_buffer_ptr;

        if (_is_symb_forbidden(c)) {
            _tree_state = TreeState::BROKEN;
            return;
        }

        if (_current_tag_content.trailed_symb_skipped == false &&
            (c != '\n' || c != '\t' || c != ' ')) {
            _current_tag_content.trailed_symb_skipped = true;
            _current_tag_content.content.clear();
            _current_tag_content.content.add(c);
        }
        else {
            _current_tag_content.content.add(c);
        }
    }
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_process_tag_end()
{
    if (_current_tag_content.in_processing) {
        _current_tag_content.trailed_symb_skipped = false;
        _current_tag_content.in_processing = false;

        _update_current_key();

        if (_current_tag == _::Tag::VALUE) {
            _fill_entry_fields();
        }
        else if (_current_tag == _::Tag::DEFAULT_SEQUENCE) {
            _fill_sequence_field();
        }
        else if (_current_tag == _::Tag::NAME) {
            _fill_group_fields();
        }

        _last_key = _current_key;
    }
    else if (_is_node_of_tree()) {
        if (_tree_deepness > 1) {
            _tree->move_out();
        }
        _tree_deepness--;
    }

    _current_tag = _last_parent_tag;
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_process_attr_content()
{
    char c = *_buffer_ptr;

    if (_is_symb_forbidden(c)) {
        _tree_state = TreeState::BROKEN;
        return;
    }

    if (_current_attr_content.in_processing) {
        _current_attr_content.content.add(c);
    }
    else {
        _current_attr_content.in_processing = true;
        _current_attr_content.content.clear();
        _current_attr_content.content.add(c);
    }
}

template<typename T1, typename T2>
inline void XmlToTreeBuilder<T1, T2>::_process_attr_end()
{
    _current_attr_content.in_processing = false;
}

template<typename T1, typename T2>
bool XmlToTreeBuilder<T1, T2>::_is_symb_forbidden(const char c)
{
    auto first_fobidden = lang::Ascii::CODES_COUNT;
    auto symb = (lang::Ascii::Type)c;

    if (symb < 0 or symb >= lang::Ascii::CODES_COUNT) {
        return true;
    }

    return false;
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_fill_entry_fields()
{
    TreeNode* currentNode = _tree->get_current_node();
    NodeContainer& container = currentNode->get_container();

    strs::StringView tag = _current_tag_content.content;
    strs::StringView attr = _current_attr_content.content;

    switch (_last_key) {
        case _::KeyType::PASSWORD: {
            char* pool_data = _allocate_in_pool(tag.data(), tag.length());
            container.set_password(pool_data, tag.length());
            container.set_password_protection(attr == _::AttrValStrings::TRUE);
        } break;

        case _::KeyType::TITLE: {
            auto deepness_str = _get_deepness_string();
            char* pool_data = _allocate_in_pool(_::MarkStrings::ENTRY);
            _allocate_in_pool(deepness_str);
            _allocate_in_pool(tag);

            container.set_name(pool_data, deepness_str.length() + tag.length() +
                                            _::MarkStrings::ENTRY.length());
        } break;

        case _::KeyType::USERNAME: {
            char* pool_data = _allocate_in_pool(tag.data(), tag.length());
            container.set_login(pool_data, tag.length());
        } break;

        default:
            break;
    }
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_fill_group_fields()
{
    TreeNode* current_node = _tree->get_current_node();
    NodeContainer& container = current_node->get_container();
    auto& tag = _current_tag_content.content;

    auto deepness_str = _get_deepness_string();
    char* pool_data = _allocate_in_pool(_::MarkStrings::GROUP);
    _allocate_in_pool(deepness_str);
    _allocate_in_pool(tag);

    container.set_name(pool_data, deepness_str.length() + tag.length() +
                                    _::MarkStrings::GROUP.length());
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_fill_sequence_field()
{
    TreeNode* current_node = _tree->get_current_node();
    NodeContainer& container = current_node->get_container();
    auto& tag = _current_tag_content.content;

    char* data = _allocate_in_pool(tag.data(), tag.length());
    container.set_sequence(data, tag.length());
}

template<typename T1, typename T2>
auto XmlToTreeBuilder<T1, T2>::_get_deepness_string() -> DeepnessString&
{
    std::size_t deepness = _tree_deepness;
    if (deepness > MAXIMAL_DEEPNESS) {
        deepness = MAXIMAL_DEEPNESS;
    }

    _deepness_string.clear();

    for (size_t i = 0; i < deepness; ++i) {
        _deepness_string += ">";
    }

    return _deepness_string;
}

template<typename T1, typename T2>
char* XmlToTreeBuilder<T1, T2>::_allocate_in_pool(const char* data,
                                                  uint32_t length)
{
    auto mem_chunk = _string_pool->allocate_n(length);
    std::memcpy(&*mem_chunk.start, data, length);
    return reinterpret_cast<char*>(&*mem_chunk.start);
}

template<typename T1, typename T2>
char* XmlToTreeBuilder<T1, T2>::_allocate_in_pool(strs::StringView s)
{
    return _allocate_in_pool(s.data(), s.length());
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_update_current_tag(_::TagType type)
{
    auto tag_name = _get_tag_name(type);
    _current_tag = _::TAG_DICT.get_value(tag_name);
}

template<typename T1, typename T2>
strs::StringView XmlToTreeBuilder<T1, T2>::_get_tag_name(_::TagType type)
{
    uint8_t* name;
    size_t name_len;

    switch (type) {
        case _::TagType::END_TAG:
            name_len = 0;
            name = (uint8_t*)_src_tree.stack + _src_tree.stacklen + 1;

            while (*name != '\0') {
                name++;
                name_len++;
            }
            name -= name_len;
            break;

        case _::TagType::START_TAG:
            name_len = yxml_symlen(&_src_tree, _src_tree.elem);
            name = (uint8_t*)_src_tree.elem;
            break;

        default:
            break;
    }

    return strs::StringView((char*)name, name_len);
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_update_current_key()
{
    strs::StringView key_name = _current_tag_content.content;
    _current_key = _::KEY_DICT.get_value(key_name);
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_update_current_attr()
{
    char* name = _src_tree.attr;
    size_t name_len = yxml_symlen(&_src_tree, _src_tree.attr);

    strs::StringView attr_name(name, name_len);
    _current_attr = _::ATTR_DICT.get_value(attr_name);
}

template<typename T1, typename T2>
void XmlToTreeBuilder<T1, T2>::_finish_tree()
{
    _tree->move_in_head();
    _tree->move_most_left();

    _tree_state = TreeState::FINISHED;
}

template<typename T1, typename T2>
inline bool XmlToTreeBuilder<T1, T2>::_is_top_group_skipped()
{
    return (_tree_deepness > 0);
}

template<typename T1, typename T2>
inline bool XmlToTreeBuilder<T1, T2>::_is_attr()
{
    return _current_attr != _::Attr::NO_ATTR;
}

template<typename T1, typename T2>
inline bool XmlToTreeBuilder<T1, T2>::_is_tag()
{
    return _current_tag != _::Tag::NO_TAG;
}

template<typename T1, typename T2>
inline bool XmlToTreeBuilder<T1, T2>::_is_contain_text()
{
    return _current_tag == _::Tag::KEY || _current_tag == _::Tag::VALUE ||
           _current_tag == _::Tag::NAME ||
           _current_tag == _::Tag::DEFAULT_SEQUENCE;
}

template<typename T1, typename T2>
inline bool XmlToTreeBuilder<T1, T2>::_is_node_of_tree()
{
    return _current_tag == _::Tag::GROUP || _current_tag == _::Tag::ENTRY;
}

template<typename T1, typename T2>
inline void XmlToTreeBuilder<T1, T2>::_check_end_of_tree()
{
    if (_current_tag == _::Tag::KEEPASS) {
        _is_tree_ended = true;
    }
}

} /* namespace tree */

#endif /* TREEBUILDER_HPP_ */
