/**
Strs library, v.1.0.3

Copyright 2019 Third Pin LLC

Permission is hereby granted, free of charge, to any person obtaining 
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
**/

#pragma once

#include "StringView.hpp"
#ifdef STRS_STD_STRINGVIEW_AVAILABLE
#include <string_view>
#endif

namespace strs {

class StringConst
{
 public:
    template<std::size_t N>
    constexpr StringConst(const char (&s)[N]) : _ptr(s), _size(N - 1)
    {
    }

    constexpr const char& operator[](std::size_t i) { return _ptr[i]; }
    constexpr const char& operator[](std::size_t i) const { return _ptr[i]; }
    constexpr const char* data() { return _ptr; }
    constexpr const char* data() const { return _ptr; }
    constexpr std::size_t length() { return _size; }
    constexpr std::size_t length() const { return _size; }
    constexpr operator const char*() { return _ptr; }
    constexpr operator const char*() const { return _ptr; }
    constexpr const char* begin() const { return _ptr; }
    constexpr const char* cbegin() const { return _ptr; }
    constexpr const char* end() const { return _ptr + _size; }
    constexpr const char* cend() const { return _ptr + _size; }

#ifdef STRS_STD_STRINGVIEW_AVAILABLE
    operator std::string_view() const
    {
        return std::string_view(data(), length());
    }
#endif

#ifndef STRS_NOT_STRINGVIEW_SUPPORT
    operator StringView() const { return StringView(data(), length()); }
#endif

 private:
    const char* const _ptr;
    const std::size_t _size;
};

}  // namespace strs
