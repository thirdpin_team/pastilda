#pragma once

#include <libopencm3/stm32/rcc.h>

namespace hw {

inline static void rcc_enable(rcc_periph_clken p)
{
    rcc_periph_clock_enable(p);
}

inline static void rcc_disable(rcc_periph_clken p)
{
    rcc_periph_clock_disable(p);
}

inline static void rcc_reset(rcc_periph_rst p)
{
    rcc_periph_reset_pulse(p);
}

/**
 * Class for working with RCC
 */
template<const auto& RCC_CLOCK_CONFIG,
         const auto& RCC_PERIPHS_FOR_ENABLE,
         const auto& RCC_PERIPHS_FOR_RESET>
struct Rcc
{
    using ClockPeriph = rcc_periph_clken;
    using ResetPeriph = rcc_periph_rst;

    /**
     * @brief Setup clock with settings from config
     */
    static void clock_setup();

    /**
     * @brief Enable periph
     */
    static void enable(ClockPeriph p) { rcc_enable(p); }

    /**
     * @brief Enable all periph provided by config
     */
    static void enable();

    /**
     * @brief Disable periph
     */
    static void disable(ClockPeriph p) { rcc_disable(p); }

    /**
     * @brief Disable all periph provided by config
     */
    static void disable();

    /**
     * @brief Reset periph
     */
    static void reset(ResetPeriph p) { rcc_reset(p); }

    /**
     * @brief Reset all periph provided by config
     */
    static void reset();
};

template<const auto& RCC, const auto& EN, const auto& RES>
void Rcc<RCC, EN, RES>::clock_setup()
{
    rcc_clock_setup_pll(&RCC);
}

template<const auto& RCC, const auto& EN, const auto& RES>
void Rcc<RCC, EN, RES>::enable()
{
    for (auto p : EN) {
        Rcc::enable(p);
    }
}

template<const auto& RCC, const auto& EN, const auto& RES>
void Rcc<RCC, EN, RES>::disable()
{
    for (auto p : EN) {
        Rcc::disable(p);
    }
}

template<const auto& RCC, const auto& EN, const auto& RES>
void Rcc<RCC, EN, RES>::reset()
{
    for (auto p : RES) {
        Rcc::reset(p);
    }
}

}  // namespace hw
