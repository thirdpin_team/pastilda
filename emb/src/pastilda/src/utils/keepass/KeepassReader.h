/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_KEEPASS_READER_H_
#define UTILS_KEEPASS_READER_H_

#include <stdint.h>
#include <string.h>

#include <hw/FileSystem.h>
#include <strs/StringFixed.hpp>

#include "KeepassCrypto.h"

namespace utils {

namespace keepass {

class KeepassReader
{
 public:
    static constexpr uint32_t MAX_PASSWORD_SIZE_IN_BYTES = 32;
    static constexpr uint32_t MAX_ENCRYPTED_PASSWORD_SIZE_IN_BYTES = 64;
    static constexpr uint32_t DECRYPTED_BLOCK_SIZE = 256;

    using Password = strs::StringFixed<MAX_ENCRYPTED_PASSWORD_SIZE_IN_BYTES>;

    KeepassReader() : _fs(FileSystem::Instance()){};
    ~KeepassReader() = default;

    enum DecryptionResult
    {
        NONE,
        SUCCESS,
        SIGNATURE_ERROR,
        MASTER_KEY_ERROR,
        CREDENTIALS_ERROR,
        DB_FILE_ERROR,
        DATA_HASH_ERROR,
        DB_FILE_CLOSED,
    };

    void set_credentials(const char* pass, uint32_t len);
    DecryptionResult check_db_file(const char* db_name);
    DecryptionResult check_db_version();
    DecryptionResult check_db_credentials();
    void read_start();
    DecryptionResult read_next_chunk(uint8_t* buffer, uint32_t* len);
    void read_stop();
    void reset_password_decryption();
    void decrypt_password(Password* password);

 private:
    using FileSystem = hw::FileSystem;
    using File = FIL;

    static constexpr uint8_t IV_SALSA[8] = {0xE8, 0x30, 0x09, 0x4B,
                                            0x97, 0x20, 0x5D, 0x2A};
    static constexpr uint32_t SIGNATURE_1 = 0x9AA2D903;
    static constexpr uint32_t SIGNATURE_2_2X_PRE_RELEASE = 0xB54BFB66;
    static constexpr uint32_t SIGNATURE_2_2X_POST_RELEASE = 0xB54BFB67;
    static constexpr uint32_t HEADER_OFFSET_IN_BYTES = 8;
    static constexpr uint32_t MAX_KEYFILE_SIZE_IN_BYTES = 32;
    static constexpr uint32_t HASH_LENGTH = 32;
    static constexpr uint32_t COMPOSITE_KEY_LENGTH = 64;
    static constexpr uint32_t MASTER_KEY_LENGTH_2X = 64;
    static constexpr uint32_t BUFFER_SIZE = 320;
    static constexpr uint32_t MAX_HEADER_FIELD_SIZE = 32;
    static constexpr uint32_t DATABASE_START = 254;

    enum HeaderFieldName : uint8_t
    {
        END_OF_HEADER = 0,
        COMMENT = 1,
        CIPHER_ID = 2,
        COMPRESSION_FLAGS = 3,
        MASTER_SEED = 4,
        TRANSFORM_SEED = 5,
        TRANSFORM_ROUNDS = 6,
        ENCRYPTION_IV = 7,
        PROTECTED_STREAM_KEY = 8,
        STREAM_START_BYTES = 9,
        INNER_RANDOM_STREAM_ID = 10,
        HEADER_FIELD_COUNT = 11
    };

    enum Credentials
    {
        NOT_SELECTED,
        PASSWORD,
        KEY_FILE,
        PASSWORD_AND_KEY_FILE
    };

#pragma pack(push, 1)
    struct HeaderField
    {
        uint16_t size;
        uint8_t data[MAX_HEADER_FIELD_SIZE];
    };

    struct BlockDataHeader
    {
        uint32_t blockID;
        uint8_t blockDataHash[HASH_LENGTH];
        uint32_t blockDataSize;
    };
#pragma pack(pop)

    struct ReadState
    {
        KeepassCrypto::ShaData sha;
        BlockDataHeader block_data;
        uint32_t block_size = 0;
        bool new_block = true;
        uint32_t offset = 0;
        bool decrypt_in_progress = false;
    };

    static ReadState _reader_state;

    FileSystem& _fs;
    File _file;

    HeaderField _header[HEADER_FIELD_COUNT];
    uint8_t _pass[MAX_PASSWORD_SIZE_IN_BYTES];
    uint32_t _pass_len = 0;
    bool _pass_reset = true;
    uint8_t _keyf[MAX_KEYFILE_SIZE_IN_BYTES];
    uint32_t _keyf_len = 0;
    uint8_t _master_key[HASH_LENGTH];
    Credentials _credentials = Credentials::NOT_SELECTED;
    uint8_t _decrypted_data[BUFFER_SIZE];

    void _readHeader();
    void _makeMasterKey(uint8_t* key, uint32_t key_len);
    void _makeMasterKey(uint8_t* pass,
                        uint8_t* keyfile,
                        uint32_t pass_len,
                        uint32_t keylile_len);
    void _makeKeyRoutine(uint8_t* key_hash);
};

} /* namespace keepass */

} /* namespace utils */

#endif /* UTILS_KEEPASS_READER_H_ */
