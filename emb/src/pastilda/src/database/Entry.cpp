/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Entry.h"

namespace db {

Entry::Entry() :
  _index(0),
  _name(EMPTY_FIELD),
  _login(EMPTY_FIELD),
  _password(EMPTY_FIELD),
  _sequence(EMPTY_FIELD),
  _password_protected(false)
{
}

Entry::Entry(size_t index) :
  _index(index),
  _name(EMPTY_FIELD),
  _login(EMPTY_FIELD),
  _password(EMPTY_FIELD),
  _sequence(EMPTY_FIELD),
  _password_protected(false)
{
}

Entry::~Entry() {}

Entry::Entry(const Entry& entry)
{
    _index = entry.get_index();
    _name = entry.get_name();
    _login = entry.get_login();
    _password = entry.get_password();
    _sequence = entry.get_sequence();
    _password_protected = false;
}

void Entry::set_index(uint32_t index)
{
    _index = index;
}

void Entry::set_password_protection(bool is_protected)
{
    _password_protected = is_protected;
}

} /* namespace db */
