/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "UsbHost.h"

namespace hw {

namespace usb {

constexpr void* UsbHost::lld_drivers[];
constexpr usbh_dev_driver_t* UsbHost::device_drivers[];

UsbHost::UsbHost(message_handler handler) :
  _timer(hw::config::USB_HOST_TIMER_NUMBER)
{
    _timer.set_prescaler_value(hw::config::USB_HOST_TIMER_PRESCALER);
    _timer.set_autoreload_value(hw::config::USB_HOST_TIMER_PERIOD);
    _timer.enable_counter();

    using Gpio = cm3cpp::gpio::Gpio;

    Gpio uf_p(config::USB_HOST_P);
    Gpio uf_m(config::USB_HOST_M);

    uf_p.mode_setup(Gpio::Mode::ALTERNATE_FUNCTION, Gpio::PullMode::NO_PULL);
    uf_p.set_af(config::USB_HOST_AF);

    uf_m.mode_setup(Gpio::Mode::ALTERNATE_FUNCTION, Gpio::PullMode::NO_PULL);
    uf_m.set_af(config::USB_HOST_AF);

    static hid_kbd_config_t kbd_config = {handler};
    hid_kbd_driver_init(&kbd_config);
    hub_driver_init();
    usbh_init(lld_drivers, device_drivers);
}

uint32_t UsbHost::get_time_us()
{
    return ((_timer.get_counter_value()) * 100);
}

void UsbHost::start()
{
    hub_driver_init();
    usbh_init(lld_drivers, device_drivers);
}

} /* namespace usb */

} /* namespace hw */
