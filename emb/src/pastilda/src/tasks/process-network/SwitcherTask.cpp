/*
 * This file is part of the pastilda project.
 * hosted at http://github.com/thirdpin/pastilda
 *
 * Copyright (C) 2016  Third Pin LLC
 * www.thirdpin.ru
 * www.thirdpin.ru/en/
 *
 * Written by:
 *  Anastasiia Lazareva <a.lazareva@thirdpin.ru>
 *	Dmitrii Lisin 		<d.lisin@thirdpin.ru>
 *	Ilya Stolyarov 		<i.stolyarov@thirdpin.ru>
 *	Pavel Larionov      <p.larionov@thirdpin.ru>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SwitcherTask.h"

namespace tasks {

// TODO: LEDS_FROM_PASTILDA handle

void SwitcherTask::Run()
{
    StateAction act;

    while (true) {
        _blocking_read();

        switch (_state) {
            case State::NORMAL:
                act = _normal_state_proc();
                if (act == StateAction::NEXT) {
                    _state = State::PASTILDA;
                }
                break;

            case State::PASTILDA:
                act = _pastilda_state_proc();
                if (act == StateAction::NEXT) {
                    _state = State::NORMAL;
                }
                break;
        }
    }
}

auto SwitcherTask::_normal_state_proc() -> StateAction
{
    if (_fifo_data.token == msg::SwitcherCommand::KEYS_FROM_HOST) {
        _last_key.set(_fifo_data.data.package.keys[0],
                      _fifo_data.data.package.special);

        if (_last_key == _pastilda_key) {
            _pastilda_key_pressed = true;

            return StateAction::STAY;
        }

        else if (_pastilda_key_pressed &&
                 (_last_key.get_kbd_key() == Key::NOT_A_KEY)) {
            _pastilda_key_pressed = false;

            LogicTask::MsgType message;
            message.token = LogicTask::MsgType::Token::PASTILDA_MODE;
            LogicTask::fifo_push(&message);

            UsbDeviceTask::enqueue(hw::kbd::ZERO_PACKAGE);

            return StateAction::NEXT;
        }

        else {
            _pastilda_key_pressed = false;

            UsbDeviceTask::enqueue(_fifo_data.data.package);
            return StateAction::STAY;
        }
    }

    if (_fifo_data.token == msg::SwitcherCommand::LEDS_FROM_DEVICE) {
        _last_led_combination = _fifo_data.data.leds;
        UsbHostTask::enqueue(_last_led_combination);
        return StateAction::STAY;
    }

    return StateAction::STAY;
}

auto SwitcherTask::_pastilda_state_proc() -> StateAction
{
    if (_fifo_data.token == msg::SwitcherCommand::KEYS_FROM_HOST) {
        _kbd_buffered_converter.process(_fifo_data.data.package);
        auto str = _kbd_buffered_converter.get_line();

        for (char c : str) {
            LogicTask::MsgType message;
            message.token = LogicTask::MsgType::Token::ASCII_KEY;
            message.data = c;
            LogicTask::fifo_push(&message);
        }

        return StateAction::STAY;
    }

    if (_fifo_data.token == msg::SwitcherCommand::KEYS_FROM_PASTILDA) {
        UsbDeviceTask::enqueue(_fifo_data.data.package);
        return StateAction::STAY;
    }

    if (_fifo_data.token == msg::SwitcherCommand::KEYS_PAUSE_FROM_PASTILDA) {
        TickType_t delay_time = rtos::Ticks::MsToTicks(_fifo_data.data.delay);

        Thread::Delay(delay_time);
        return StateAction::STAY;
    }

    if (_fifo_data.token == msg::SwitcherCommand::LEDS_FROM_PASTILDA) {
        UsbHostTask::enqueue(_fifo_data.data.leds);
        return StateAction::STAY;
    }

    if (_fifo_data.token == msg::SwitcherCommand::PASTILD_END) {
        UsbHostTask::enqueue(_last_led_combination);
        return StateAction::NEXT;
    }

    return StateAction::STAY;
}

} /* namespace tasks */
